<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/csapatok/szeged', 'SzegedController@asd');
Route::get('adminszeged', 'SzegedController@szegedadmin')->name('admin.szeged');
Route::delete('/adminszeged/{id}', 'SzegedController@destroy')->name('szeged.torol');
Route::get('/szegedcreate', 'SzegedController@index')->middleware('auth:admin');
Route::post('/szegedcreate', 'SzegedController@store')->name('addszeged');
Route::get('/szegededit/{id}', 'SzegedController@edit')->middleware('auth:admin');
Route::put('/szegededit/{id}', 'SzegedController@update')->middleware('auth:admin');


Route::get('/csapatok/veszprem', 'VeszpremController@veszpremi');
Route::get('/Pages/veszpremi', 'VeszpremController@veszpremcreate')->middleware('auth:admin');


Route::get('/csapatok/siofok', 'SiofokController@sio');
Route::get('adminsiofok', 'SiofokController@siofokadmin')->name('admin.siofok')->middleware('auth:admin');
Route::delete('/adminsiofok/{id}', 'SiofokController@destroy')->name('siofok.torol');
Route::get('/siofokcreate', 'SiofokController@index')->middleware('auth:admin');
Route::post('/siofokcreate', 'SiofokController@store')->name('addsio');
Route::get('/siofokedit/{id}', 'SiofokController@edit')->middleware('auth:admin');
Route::put('/siofokedit/{id}', 'SiofokController@update')->middleware('auth:admin');


Route::get('/csapatok/gyor', 'GyorController@gyor');
Route::get('/Pages/gyori', 'GyorController@create')->middleware('auth:admin');


Route::get('/csapatok/fradi', 'FradiController@fradi');
Route::get('/Pages/fradii', 'FradiController@fradicreate')->middleware('auth:admin');


Route::get('/tabella', 'TabellaController@magyarf');
Route::get('/hirek', 'HirekController@hir');
Route::get('/', 'IndexController@valami');


Route::get('/europabajnoksag', 'VersenyekController@eb');
Route::get('/vilagbajnoksag', 'VersenyekController@vb');
Route::get('/europaikupak', 'VersenyekController@elbl');


Route::get('profil', 'UserController@profile')->middleware('verified');
Route::post('profil', 'UserController@update_avatar');



Route::get('/shop','ShopController@index')->name('shop');
Route::get('/shop/{id}','ShopController@show')->name('shop.show');
Route::get('/checkout','CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::get('/guestcheckout','CheckoutController@index')->name('guestcheckout.index');
Route::post('/checkout','CheckoutController@store')->name('checkout.store');
Route::get('/thankyou','ConfirmationController@index')->name('confirmation.index');
Route::get('/cart','CartController@index')->name('cart.index');
Route::post('/cart','CartController@store')->name('cart.store');
Route::delete('/cart/{product}','CartController@destroy')->name('cart.destroy');
Route::patch('/cart/{product}','CartController@update')->name('cart.update');
Route::get('empty', function (){
    Cart::destroy();
});


Route::get('/order','OrderController@index')->name('admin.order');
Route::get('/order/update', 'OrderController@updateStatus')->name('order.update.status');





Route::get('eredmenyek/{date?}','EredmenyekController@eredmenyek');








Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');

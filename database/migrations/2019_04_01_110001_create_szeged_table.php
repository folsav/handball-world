<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSzegedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('szeged', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nev');
            $table->string('poszt');
            $table->string('nemzet');
            $table->integer('kor');
            $table->integer('magassag');
            $table->date('szulido');
            $table->string('szulhely');
            $table->string('kep');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('szeged');
    }
}

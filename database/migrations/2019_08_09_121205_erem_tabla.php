<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EremTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eremtabla', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('helyezes');
            $table->string('nemzet');
            $table->integer('arany');
            $table->integer('ezust');
            $table->integer('bronz');
            $table->integer('osszesen');
            $table->string('nem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eremtabla');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eb', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vbev');
            $table->string('rendezo');
            $table->string('arany');
            $table->integer('eredmenyelso');
            $table->integer('eredmenymasodik');
            $table->string('ezust');
            $table->string('bronz');
            $table->integer('eredmenyharmadik');
            $table->integer('eredmenynegyedik');
            $table->string('negyedik');
            $table->string('legjobbjatekos');
            $table->string('golkiraly');
            $table->integer('golok');
            $table->string('nem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eb');
    }
}

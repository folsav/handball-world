<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGyorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gyor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('type');
            $table->date('date_of_birth');
            $table->string('nationality');
            $table->integer('height');
            $table->string('jersey_number');
            $table->string('img');
            $table->integer('kor');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('gyor');
    }
}

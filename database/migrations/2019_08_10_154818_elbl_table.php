<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ElblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elbl', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ev');
            $table->string('arany');
            $table->integer('eredmenyelso');
            $table->integer('eredmenymasodik');
            $table->string('ezust');
            $table->string('bronz');
            $table->string('negyedik');
            $table->string('legjobbjatekos');
            $table->string('golkiraly');
            $table->integer('golok');
            $table->string('verseny');
            $table->string('nem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elbl');
    }
}

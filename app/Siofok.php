<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siofok extends Model
{
    protected $table = "siofok";
    protected $fillable = ['nev','poszt','mez','nemzet','kor','magassag','szulido','szulhey','kep'];
}

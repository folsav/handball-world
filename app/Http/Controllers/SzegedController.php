<?php

namespace App\Http\Controllers;

use App\Szeged;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class SzegedController extends Controller
{
    //

    public function asd(){

        $Szeged = Szeged::All();


        return view('Pages.szeged', compact('Szeged'));
    }

    public function szegedadmin(){
        //$Szeged = Szeged::All();
        $Szeged = DB::table('szeged')->paginate('7');

        return view('multiauth::admin.szegedadmin', compact('Szeged'));
    }
    public function destroy($id){

        $Szeged = Szeged::find($id);
        $Szeged->delete();

        return redirect('/adminszeged')->with('success', 'Játékos sikeresen törölve');
    }

    public function index(){

        return view('multiauth::admin.szegedcreate');
    }

    public function store(Request $request){

        $Szeged = new Szeged();

        $Szeged->nev = $request->input('nev');
        $Szeged->poszt = $request->input('poszt');
        $Szeged->mez = $request->input('mez');
        $Szeged->nemzet = $request->input('nemzet');
        $Szeged->kor = $request->input('kor');
        $Szeged->magassag = $request->input('magassag');
        $Szeged->szulido = $request->input('szulido');
        $Szeged->szulhely = $request->input('szulhely');
        if ($request->hasFile('kep')) {
            $file = $request->file('kep');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save(public_path('/images/pick/' . $filename));
            $Szeged->kep = $filename;
        }
        $Szeged->Csillagjegy = $request->input('Csillagjegy');
        $Szeged->csaladiallapot = $request->input('csaladiallapot');
        $Szeged->elozoklubbok = $request->input('elozoklubbok');
        $Szeged->valogatottsag = $request->input('valogatottsag');
        $Szeged->hobbi = $request->input('hobbi');
        $Szeged->orszag = $request->input('orszag');
        $Szeged->varos = $request->input('varos');
        $Szeged->csapat = $request->input('csapat');
        $Szeged->jatekos = $request->input('jatekos');
        $Szeged->szinesz = $request->input('szinesz');
        $Szeged->auto = $request->input('auto');

        $Szeged->save();


        return redirect('adminszeged')->with('szeged',$Szeged)->with('success', 'Játékos sikeresen hozzáadva!');
    }

    public function edit($id){
        $Szeged = Szeged::find($id);
        return view('multiauth::admin.szegededit')->with('szeged',$Szeged);
    }
    public function update(Request $request, $id){

        $Szeged = Szeged::find($id);

        $Szeged->nev = $request->input('nev');
        $Szeged->poszt = $request->input('poszt');
        $Szeged->mez = $request->input('mez');
        $Szeged->nemzet = $request->input('nemzet');
        $Szeged->kor = $request->input('kor');
        $Szeged->magassag = $request->input('magassag');
        $Szeged->szulido = $request->input('szulido');
        $Szeged->szulhely = $request->input('szulhely');
        if ($request->hasFile('kep')) {
            $file = $request->file('kep');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save(public_path('/images/pick/' . $filename));
            $Szeged->kep = $filename;
        }
        $Szeged->Csillagjegy = $request->input('Csillagjegy');
        $Szeged->csaladiallapot = $request->input('csaladiallapot');
        $Szeged->elozoklubbok = $request->input('elozoklubbok');
        $Szeged->valogatottsag = $request->input('valogatottsag');
        $Szeged->hobbi = $request->input('hobbi');
        $Szeged->orszag = $request->input('orszag');
        $Szeged->varos = $request->input('varos');
        $Szeged->csapat = $request->input('csapat');
        $Szeged->jatekos = $request->input('jatekos');
        $Szeged->szinesz = $request->input('szinesz');
        $Szeged->auto = $request->input('auto');

        $Szeged->save();

        return redirect('adminszeged')->with('szeged'.$Szeged)->with('success', 'Játékos sikeresen szerkesztve');

    }
}

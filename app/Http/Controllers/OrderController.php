<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();

        return view('multiauth::admin.order', compact('orders'));
    }

    public function updateStatus(Request $request)
    {
        $orders = Order::findOrFail($request->id);
        $orders->shipped = $request->shipped;
        $orders->save();

        return response()->json(['message' => 'User status updated successfully.']);
    }

}

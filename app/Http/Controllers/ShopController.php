<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ShopController extends Controller
{
    public function index(){

        if (request()->category) {
            $products = Product::with('categories')->whereHas('categories', function ($query) {
                $query->where('slug', request()->category);
            })->get();
            $categories = Category::all();
        } else {
            $products = Product::inRandomOrder()->take(10)->get();
            $categories = Category::all();
        }

        return view('shop.shop')->with([
            'porducts' => $products,
            'categories' => $categories,
        ]);
    }
    public function show($id){
        $products = Product::where('id', $id)->firstOrFail();
        $mightAlsoLike = Product::where('id', '!=', $id)->inRandomOrder()->take(4)->get();


        return view('shop.product')->with([
            'product' => $products,
            'mightAlsoLike' => $mightAlsoLike,
        ]);
    }
}

<?php

namespace App\Http\Controllers;
use App\Szulhely;
use App\Veszprem;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;


class VeszpremController extends Controller{

    public function veszpremcreate()
    {
        $Szuletesihely = Szulhely::All();

        $client = new Client();

        $key = env('SPORTRADAR_KEY');

        try {
            $body = $client->get('http://api.sportradar.us/handball/trial/v2/en/competitors/sr:competitor:6628/profile.json?api_key=y62asfx7v2hq4u9fyunxdhsh&fbclid'
                . $key,
                [

                ]
            )->getBody()->getContents();
        } catch (ClientException $e) {
            return "Erroresq. ";              //TO DO: itt kéne tovább irányitani az error oldalra
        }

        $players = json_decode($body, true);

        foreach ($players['players'] as &$valtozo){
            switch($valtozo['type']){
                case "CB":
                    $valtozo['type'] = "Irányító";
                break;
                case "LB":
                    $valtozo['type'] = "Balátlövő";
                    break;
                case "P":
                    $valtozo['type'] = "Beálló";
                    break;
                case "RB":
                    $valtozo['type'] = "Jobbátlövő";
                    break;
                case "RW":
                    $valtozo['type'] = "Jobbszélső";
                    break;
                case "LW":
                    $valtozo['type'] = "Balszélső";
                    break;
                case "G":
                    $valtozo['type'] = "Kapus";
                    break;
            }

        }
        foreach ($players['players'] as &$valtozo){
            switch($valtozo['nationality']){
                case "France":
                    $valtozo['nationality'] = "Francia";
                    break;
                case "Serbia":
                    $valtozo['nationality'] = "Szerb";
                    break;
                case "Denmark":
                    $valtozo['nationality'] = "Dán";
                    break;
                case "Sweden":
                    $valtozo['nationality'] = "Svéd";
                    break;
                case "Slovenia":
                    $valtozo['nationality'] = "Szlovén";
                    break;
                case "Norway":
                    $valtozo['nationality'] = "Norvég";
                    break;
                case "Croatia":
                    $valtozo['nationality'] = "Horvát";
                    break;
                case "Hungary":
                    $valtozo['nationality'] = "Magyar";
                    break;
                case "Bosnia & herzegovina":
                    $valtozo['nationality'] = "Bosznia Hercegovina";
                    break;
                case "Spain":
                    $valtozo['nationality'] = "Spanyol";
                    break;
            }
            if (!isset($valtozo['height'])) {
                $valtozo['height'] = "178";
            }

            $slug = Str::slug($valtozo['name'], '_');
            $valtozo['img'] = $slug.'.jpg';

            $slug = Str::slug($valtozo['name'], '_');
            $valtozo['slug'] = $slug;

            $age = date_diff(date_create($valtozo['date_of_birth']), date_create('now'))->y;
            $valtozo['kor'] = $age;

            foreach ($Szuletesihely as $szulihely){
                if($szulihely['slug'] == $valtozo['slug']){
                    $valtozo['szuletesihely'] = $szulihely['szulhely'];
                }
            }

        }

        Veszprem::truncate();

        foreach ($players['players'] as &$valtozo){
            $veszprem = new Veszprem();

            $veszprem['name'] = $valtozo['name'];
            $veszprem['type'] = $valtozo['type'];
            $veszprem['date_of_birth'] = $valtozo['date_of_birth'];
            $veszprem['nationality'] = $valtozo['nationality'];
            $veszprem['height'] = $valtozo['height'];
            if (!isset($valtozo['jersey_number'])){
                $valtozo['jersey_number'] = '0';
            }
            $veszprem['jersey_number'] = $valtozo['jersey_number'];
            $veszprem['img'] = $valtozo['img'];
            $veszprem['kor'] = $valtozo['kor'];
            $veszprem['slug'] = $valtozo['slug'];
            if (!isset($valtozo['szuletesihely'])){
                $valtozo['szuletesihely'] = 'Nem ismert';
            }
            $veszprem['szuletesihely'] = $valtozo['szuletesihely'];
            $veszprem->save();

        }
        return view('multiauth::admin.home');


    }
    public function veszpremi(){
        $Veszprem = Veszprem::All();

        return view('Pages.veszprem', compact('Veszprem'));

    }
}


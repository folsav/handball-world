<?php

namespace App\Http\Controllers;

use DateTime;
use DateTimeZone;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mockery\Matcher\Not;

class EredmenyekController extends Controller
{
    public function eredmenyek($date = null)
    {

        $today = date("Y-m-d");
        if ($date == null){
            $date = $today;
        }

        $client = new Client();
        $key = env('SPORTRADAR_KEY');

        try {
            $body = $client->get("http://api.sportradar.us/handball/trial/v2/en/schedules/$date/summaries.json?api_key=cwzh255b6a3ndsnzzhgzh5xe"
                . $key,
                [

                ]
            )->getBody()->getContents();
        } catch (ClientException $e) {
            return "Erroresq. ";              //TO DO: itt kéne tovább irányitani az error oldalra
        }

        $eredmeny = json_decode($body, true);


        $meccsek = [];

        //return $eredmeny;


        foreach ($eredmeny['summaries'] as $k => $value){

            $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['country']['country'] =$value['sport_event']['sport_event_context']['category']['name'];
            $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['country']['slug'] =Str::slug($value['sport_event']['sport_event_context']['category']['name'],'_').'.png';
            $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k] =  array();

            $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['season'] = $value['sport_event']['sport_event_context']['season']['name'];
            $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['start_time'] = substr(str_replace('T', ' ',$value['sport_event']['start_time']),0, -9);
            $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['start_time_time'] = str_replace(' ','',strstr(substr(str_replace('T', ' ',$value['sport_event']['start_time']),0, -9), ' '));


            $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['status'] = $value['sport_event_status']['status'];
            switch($meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['status']){
                case "postponed":
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['status'] = "Elhalasztva";
                    break;
                case "closed":
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['status'] = "Vége";
                    break;
                case "not_started":
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['status'] = $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['start_time_time'];
                    break;
                case "cancelled":
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['status'] = "Lezárult";
                    break;
                case "live":
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['status'] = "Élő";
                    break;
            }
            if (!isset($value['sport_event']['venue']['name'])) {
                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['arena_name'] = '';
            }
            if (!isset($value['sport_event']['venue']['capacity'])) {
                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['capacity'] = '';
            }
            if (!isset($value['sport_event']['venue']['city_name'])) {
                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['city_name'] = '';
            }
            if (isset($value['sport_event']['venue'])){
                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['arena_name'] = $value['sport_event']['venue']['name'];
                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['capacity'] = $value['sport_event']['venue']['capacity'];
                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k]['city_name'] = $value['sport_event']['venue']['city_name'];
            }


            if (!isset($value['sport_event_status']['home_score'])) {
                $value['sport_event_status']['home_score'] = "";
            }
            if (!isset($value['sport_event_status']['away_score'])) {
                $value['sport_event_status']['away_score'] = "";
            }


            foreach ($value['sport_event']['competitors'] as $a => $ertek){
                if (!isset($ertek['country'])) {
                    $ertek['country'] = "";
                }
                if (!isset($ertek['country_code'])) {
                    $ertek['country_code'] = "";
                }

                if ($ertek['qualifier'] == 'home'){
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['home'] = $ertek['name'];
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['img'] = Str::slug($ertek['name'],'_').'.png';
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['home_country'] = $ertek['country'];
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['country_code'] = $ertek['country_code'];
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['home_score'] =  $value['sport_event_status']['home_score'];

                }else {
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['away'] = $ertek['name'];
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['img'] = Str::slug($ertek['name'],'_').'.png';
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['away_country'] = $ertek['country'];
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['country_code'] = $ertek['country_code'];
                    $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['away_score'] =  $value['sport_event_status']['away_score'];
                }
                if (isset($value['sport_event_status']['period_scores'])) {
                    foreach ($value['sport_event_status']['period_scores'] as $period_scores) {


                        if ($ertek['qualifier'] == 'away') {

                            if ($period_scores['number'] == '1'){
                                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['away_score_period_first'] = $period_scores['away_score'];
                            }
                            if ($period_scores['number'] == '2'){
                                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['away_score_period_second'] = $period_scores['away_score'];
                            }
                        }else{
                            if ($period_scores['number'] == '1'){
                                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['home_score_period_first'] = $period_scores['home_score'];
                            }
                            if ($period_scores['number'] == '2'){
                                $meccsek[$value['sport_event']['sport_event_context']['competition']['name']]['list'][$k][$a]['home_score_period_second'] = $period_scores['home_score'];
                            }
                        }
                    }
                }
            }

        }

        //return $meccsek;
        return view('Pages.eredmenyek', compact('meccsek', 'date'));
    }

}

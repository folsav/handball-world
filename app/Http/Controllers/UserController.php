<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Order;
use App\OrderProduct;
use App\Product;
use Auth;
use Image;
use DB;
use Carbon\Carbon;

class UserController extends Controller
{
    //
    public function profile()
    {
        $data = DB::table('orders')
                ->join('order_product', 'orders.id', '=', 'order_product.order_id')
                ->join('products', 'products.id', '=', 'order_product.product_id')
                ->select('products.name','orders.billing_total','orders.payment_gateway','orders.shipped'
                ,'orders.created_at', 'order_product.quantity')
                ->get();


        foreach ($data as &$valtozo) {
                switch($valtozo->shipped){
                    case "0":
                        $valtozo->shipped = "Előkészítés alatt";
                        break;
                    case "1":
                        $valtozo->shipped = "A csomag úton";
                        break;
                }
        }
        return view('profil', compact('data'), array('user' => Auth::user()));
    }

    public function update_avatar(Request $request)
    {
        // Handle the user upload of avatar
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/images/profil/' . $filename));
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        return view('profil', array('user' => Auth::user()));
    }


}

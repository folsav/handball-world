<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use willvincent\Feeds\Facades\FeedsFacade;
use Feeds;


class HirekController extends Controller
{
    public function hir()
    {
        $feed = Feeds::make('http://www.nemzetisport.hu/static/rss_cikkek_kezilabda.xml');

        $data = array(
            'title' => $feed->get_title(),
            'permalink' => $feed->get_permalink(),
            'items' => $feed->get_items(),
        );

        $var = rand(1,4);


        return View::make('Pages.hirek', $data, compact('var'));
        //return $data;
    }



}

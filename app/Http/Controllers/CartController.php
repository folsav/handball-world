<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function index(){

        $mightAlsoLike = Product::inRandomOrder()->take(4)->get();

        return view('shop.cart')->with('mightAlsoLike', $mightAlsoLike);
    }

    public function store(Request $request){

        $duplicates = Cart::search(function($cartItem, $rowId) use ($request){
            return $cartItem->id === $request->id;
        });

        if($duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('success_message', 'Már a kosárban van');
        }

        Cart::add($request->id, $request->name, 1, $request->price)
            ->associate('App\Product');

        return redirect()->route('cart.index')->with('success_message', 'Sikeresen hozzáadva a kosár tartalmához!');
    }

    public function empty(){
        Cart::destroy();
    }

    public function destroy($id){
        Cart::remove($id);

        return back()->with('success_message', 'Termék törölve');

    }

    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);
        if ($validator->fails()) {
            session()->flash('errors', collect(['A darabszámnak 1 és 5 között kell lennie!']));
            return response()->json(['success' => false], 400);
        }

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'A mennyiség sikeresen frissítve!');
        return response()->json(['success' => true]);
    }
}

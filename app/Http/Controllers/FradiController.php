<?php

namespace App\Http\Controllers;
use App\Fradi;
use App\Szulhely;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Mockery\Matcher\Not;

class FradiController extends Controller
{
    public function fradicreate()
    {
        $Szuletesihely = szulhely::All();

        $client = new Client();

        $key = env('SPORTRADAR_KEY');

        try {
            $body = $client->get('http://api.sportradar.us/handball/trial/v2/en/competitors/sr:competitor:6948/profile.json?api_key=y62asfx7v2hq4u9fyunxdhsh&fbclid'
                . $key,
                [

                ]
            )->getBody()->getContents();
        } catch (ClientException $e) {
            return "Erroresq. ";              //TO DO: itt kéne tovább irányitani az error oldalra
        }

        $players = json_decode($body, true);


        foreach ($players['players'] as &$valtozo) {
            if (!isset($valtozo['type'])) {
                $valtozo['type'] = "nincsen";
            } else {
                switch($valtozo['type']){
                    case "CB":
                        $valtozo['type'] = "Irányító";
                        break;
                    case "LB":
                        $valtozo['type'] = "Balátlövő";
                        break;
                    case "P":
                        $valtozo['type'] = "Beálló";
                        break;
                    case "RB":
                        $valtozo['type'] = "Jobbátlövő";
                        break;
                    case "RW":
                        $valtozo['type'] = "Jobbszélső";
                        break;
                    case "LW":
                        $valtozo['type'] = "Balszélső";
                        break;
                    case "G":
                        $valtozo['type'] = "Kapus";
                        break;
                }
            }
        }
        foreach ($players['players'] as &$valtozo){
            if (!isset($valtozo['nationality'])) {
                $valtozo['nationality'] = "Magyar";
            } else {
                switch ($valtozo['nationality']) {
                    case "France":
                        $valtozo['nationality'] = "Francia";
                        break;
                    case "Denmark":
                        $valtozo['nationality'] = "Dán";
                        break;
                    case "Romania":
                        $valtozo['nationality'] = "Román";
                        break;
                    case "Slovenia":
                        $valtozo['nationality'] = "Szlovén";
                        break;
                    case "Norway":
                        $valtozo['nationality'] = "Norvég";
                        break;
                    case "Brazil":
                        $valtozo['nationality'] = "Brazil";
                        break;
                    case "Hungary":
                        $valtozo['nationality'] = "Magyar";
                        break;
                    case "Netherlands":
                        $valtozo['nationality'] = "Holland";
                        break;
                    case "Czech republic":
                        $valtozo['nationality'] = "Cseh";
                        break;
                }
            }
            if (!isset($valtozo['date_of_birth'])) {
                $valtozo['date_of_birth'] = "1993-12-23.";
            }
            if (!isset($valtozo['height'])) {
                $valtozo['height'] = "178";
            }

            $slug = Str::slug($valtozo['name'], '_');
            $valtozo['img'] = $slug.'.jpg';

            $slug = Str::slug($valtozo['name'], '_');
            $valtozo['slug'] = $slug;

            $age = date_diff(date_create($valtozo['date_of_birth']), date_create('now'))->y;
            $valtozo['kor'] = $age;

            foreach ($Szuletesihely as $szulihely){
                if($szulihely['slug'] == $valtozo['slug']){
                    $valtozo['szuletesihely'] = $szulihely['szulhely'];
                }
            }
        }



        Fradi::truncate();

        foreach ($players['players'] as &$valtozo){
            $fradi = new Fradi();

            $fradi['name'] = $valtozo['name'];
            $fradi['type'] = $valtozo['type'];
            $fradi['date_of_birth'] = $valtozo['date_of_birth'];
            $fradi['nationality'] = $valtozo['nationality'];
            $fradi['height'] = $valtozo['height'];
            if (!isset($valtozo['jersey_number'])){
                $valtozo['jersey_number'] = '0';
            }
            $fradi['jersey_number'] = $valtozo['jersey_number'];
            $fradi['img'] = $valtozo['img'];
            $fradi['kor'] = $valtozo['kor'];
            $fradi['slug'] = $valtozo['slug'];
            if (!isset($valtozo['szuletesihely'])){
                $valtozo['szuletesihely'] = 'Nem ismert';
            }
            $fradi['szuletesihely'] = $valtozo['szuletesihely'];

            $fradi->save();


        }
        return view('multiauth::admin.friss');
    }

    public function fradi(){
        $Fradi = fradi::All();

        return view('Pages.fradi', compact('Fradi'));

    }
}

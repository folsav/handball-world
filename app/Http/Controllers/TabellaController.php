<?php

namespace App\Http\Controllers;
use App\Tabella;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TabellaController extends Controller
{
    public function magyarf()
    {

        $client = new Client();

        $key = env('SPORTRADAR_KEY');

        try {
            $body = $client->get('http://api.sportradar.us/handball/trial/v2/en/seasons/sr:season:56468/standings.json?api_key=cwzh255b6a3ndsnzzhgzh5xe'
                . $key,
                [

                ]
            )->getBody()->getContents();
        } catch (ClientException $e) {
            return "Erroresq. ";              //TO DO: itt kéne tovább irányitani az error oldalra
        }
        $nb1 = json_decode($body, true);

        foreach ($nb1['standings'][0]['groups'][0]['standings'] as &$valtozo){
        $slug = Str::slug($valtozo['competitor']['name'], '_');
        $valtozo['competitor']['img'] = $slug.'.png';
        }
        foreach ($nb1['standings'][1]['groups'][0]['standings'] as &$valtozo){
            $slug = Str::slug($valtozo['competitor']['name'], '_');
            $valtozo['competitor']['img'] = $slug.'.png';
        }

        $nb1['nevminta'] = explode(" ", $nb1['standings'][0]['groups'][0]['name']);
        $nb1['standings'][0]['nevminta'] = $nb1['nevminta'][0]." ".$nb1['nevminta'][1];

        $slug = Str::slug($nb1['standings'][0]['nevminta'], '_');
        $nb1['standings'][0]['bajnoksag'] = $slug.'.png';

        try {
            $bundes = $client->get('http://api.sportradar.us/handball/trial/v2/en/seasons/sr:season:55149/standings.json?api_key=z9sx9b3fu3paktdxftb8d4vk'
                . $key,
                [

                ]
            )->getBody()->getContents();
        } catch (ClientException $e) {
            return "Erroresq. ";              //TO DO: itt kéne tovább irányitani az error oldalra
        }


        $nemet = json_decode($bundes, true);

        foreach ($nemet['standings'][0]['groups'][0]['standings'] as &$valtozo){
            $slug = Str::slug($valtozo['competitor']['name'], '_');
            $valtozo['competitor']['img'] = $slug.'.png';
        }
        foreach ($nemet['standings'][1]['groups'][0]['standings'] as &$valtozo){
            $slug = Str::slug($valtozo['competitor']['name'], '_');
            $valtozo['competitor']['img'] = $slug.'.png';
        }

        $nemet['nevminta'] = explode(" ", $nemet['standings'][0]['groups'][0]['name']);
        $nemet['standings'][0]['nevminta'] = $nemet['nevminta'][0];

        $slug = Str::slug($nemet['standings'][0]['nevminta'], '_');
        $nemet['standings'][0]['bajnoksag'] = $slug.'.png';


        //return $nb1;
        return view('Pages.tabella', compact('nb1','nemet'));

    }


}




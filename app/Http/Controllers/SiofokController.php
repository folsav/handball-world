<?php

namespace App\Http\Controllers;

use App\Siofok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;





class SiofokController extends Controller
{
    public function sio(){

        $Siofok = Siofok::All();


        return view('Pages.siofok', compact('Siofok'));
    }

    public function siofokadmin(){
       // $Siofok = Siofok::All();
        $Siofok = DB::table('siofok')->paginate('7');

        return view('multiauth::admin.siofokadmin', compact('Siofok'));
    }


    public function destroy($id){

        $Siofok = Siofok::find($id);
        $Siofok->delete();

        return redirect('/adminsiofok')->with('success', 'Játékos sikeresen törölve');
    }

    public function index(){

        return view('multiauth::admin.siofokcreate');
    }

    public function store(Request $request){

        $siofok = new Siofok();

        $siofok->nev = $request->input('nev');
        $siofok->poszt = $request->input('poszt');
        $siofok->mez = $request->input('mez');
        $siofok->nemzet = $request->input('nemzet');
        $siofok->kor = $request->input('kor');
        $siofok->magassag = $request->input('magassag');
        $siofok->szulido = $request->input('szulido');
        $siofok->szulhey = $request->input('szulhey');

        if ($request->hasFile('kep')) {
            $file = $request->file('kep');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save('images/sio/' . $filename);
            $siofok->kep = $filename;
        }

        $siofok->save();


        return redirect('adminsiofok')->with('siofok',$siofok)->with('success', 'Játékos sikeresen hozzáadva!');
    }

    public function edit($id){
        $siofok = Siofok::find($id);
        return view('multiauth::admin.siofokedit')->with('siofok',$siofok);
    }
    public function update(Request $request, $id){

        $siofok = Siofok::find($id);

        $siofok->nev = $request->input('nev');
        $siofok->poszt = $request->input('poszt');
        $siofok->mez = $request->input('mez');
        $siofok->nemzet = $request->input('nemzet');
        $siofok->kor = $request->input('kor');
        $siofok->magassag = $request->input('magassag');
        $siofok->szulido = $request->input('szulido');
        $siofok->szulhey = $request->input('szulhey');

        if ($request->hasFile('kep')) {
            $file = $request->file('kep');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save('images/sio/' . $filename);
            $siofok->kep = $filename;
        }

        $siofok->save();

        return redirect('adminsiofok')->with('siofok'.$siofok)->with('success', 'Játékos sikeresen szerkesztve');

    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veszprem extends Model
{
    protected $table = "veszprem";
    protected $fillable = ['name','type','date_of_birth','nationality','height','jersey_number','img','kor','slug','szuletesihely'];
}

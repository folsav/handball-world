<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Szeged extends Model
{
    protected $table = "szeged";
    protected $fillable = ['nev','poszt','mez','nemzet','kor','magassag','szulido','szulhely','kep','Csillagjegy','csaladiallapot','elozoklubbok','valogatottsag','hobbi','orszag','varos','csapat','jatekos','szinesz','auto'];
}

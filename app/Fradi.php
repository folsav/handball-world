<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fradi extends Model
{
    protected $table = "fradi";
    protected $fillable = ['name','type','date_of_birth','nationality','height','jersey_number','img','kor','slug','szuletesihely'];
}

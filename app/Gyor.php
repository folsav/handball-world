<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gyor extends Model
{
    protected $table = "gyor";
    protected $fillable = ['name','type','date_of_birth','nationality','height','jersey_number','img','kor','slug','szuletesihely'];

}

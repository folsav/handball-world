@extends('layout')
@section('title')
    Profil
@endsection
@section('menu')
    Profil
@endsection
@section('cim')
    Profil
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" style="padding-top: 50px">
                <img src="{{ asset('images/profil') }}/{{$user->avatar}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
                <h2>{{ $user->name }} Profilja</h2>
                <form enctype="multipart/form-data" action="/profil" method="POST" >
                    <label>Profilkép feltöltése</label>
                    <input type="file" name="avatar"  style="display: block">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <br>
                    <input type="submit" class="pull-right btn btn-md btn-primary">
                </form>
            </div>
        </div>
        <main class="py-4">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                            <div class="card-body">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Rendelés ideje</th>
                                        <th scope="col">Termék</th>
                                        <th scope="col">Mennyiség</th>
                                        <th scope="col">Összeg</th>
                                        <th scope="col">Fizetés Típusa</th>
                                        <th scope="col">Státusz</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i=0;
                                    @endphp
                                    <?php
                                    use Carbon\Carbon;
                                    ?>
                                    @foreach($data as $order)
                                        <tr>
                                            <th scope="row"><?php $i++; echo $i; ?></th>
                                            <td>{{ Carbon::parse($order->created_at)->diffForHumans() }}</td>
                                            <td>{{$order->name}}</td>
                                            <td>{{$order->quantity}}</td>
                                            <td>{{ $order->billing_total }} HUF</td>
                                            <td>{{ $order->payment_gateway }}</td>
                                            <td>
                                                @if($order->shipped == "Előkészítés alatt")
                                                    <i class="fas fa-box-open"></i> {{ $order->shipped}}
                                                    @else
                                                    <i class="fas fa-truck"></i> {{ $order->shipped}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
        </main>
    </div>
@endsection

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title', 'Handball World')</title>
    <link rel="icon" type="image/png" href="{{ asset('images/kezilogo.png') }}"/>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>


<body>


<header>
    <nav>
        <label for="drop" class="toggle">&#8801; Handball-World<img src="{{ asset('images/kezilogozold.png') }}" alt="" style="width: 60px;float: right"></label>
        <input type="checkbox" id="drop" />
        <ul class="menu">
            <li class="home">
                <a href="#"><i class="fas fa-home"></i> Home</a>
            </li>
            <li>
                <a href="/eredmenyek"><i class="fas fa-poll"></i> Erdemények</a>
            </li>


            <li>
                <a href="/tabella"><i class="fas fa-table"></i> Tabellák</a>
            </li>

            <li>
                <a href="/hirek"><i class="fas fa-newspaper"></i> Hírek</a>
            </li>

            <li>
                <a href="/forums"><i class="fas fa-file-alt"></i> Fórum</a>
            </li>


            <li class="logo_area">
                <a href="/"><img src="{{ asset('images/kezilogozold.png') }}" alt=""></a>
            </li>

            @guest
            <li>
                <!-- First Tier Drop Down -->
                <a href="{{ route('login') }}"><i class="fa fa-user"></i> Belépés</a>
                <input type="checkbox" id="drop-1"/>
                @else
                    <li>
                        <label for="drop-1" class="toggle"><i class="fa fa-user"></i> {{ Auth::user()->name }}</label>
                        <a href="#">{{ Auth::user()->name }} <i class="fa fa-caret-down "></i></a>
                        <input type="checkbox" id="drop-1"/>
                        <ul style="position: fixed">
                            <li>
                                <a href="/profil"><i class="fas fa-user"></i>Profil</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i>
                                    Kilép
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
            </li>
            @endguest
            <li>
                <a href="/shop"><i class="fas fa-cart-arrow-down"></i> Shop</a>
            </li>

            <li>
                <!-- First Tier Drop Down -->
                <label for="drop-2" class="toggle"><i class="fas fa-volleyball-ball"></i> Csapatok</label>
                <a href="#"><i class="fas fa-volleyball-ball"></i> Csapatok <i class="fa fa-caret-down "></i></a>
                <input type="checkbox" id="drop-2"/>
                <ul>
                    <li>
                        <!-- Second Tier Drop Down -->
                        <label for="drop-3" class="toggle">Női</label>
                        <label for="drop-4" class="toggle">Férfi</label>
                        <a href="#">Női</a>
                        <input type="checkbox" id="drop-3"/>
                        <ul>
                            <li><a href="/csapatok/fradi">FTC-Rail Cargo Hungaria</a></li>
                            <li><a href="/csapatok/gyor">Győri Audi ETO KC</a></li>
                            <li><a href="/csapatok/siofok">Siófok KC</a></li>
                        </ul>
                    </li>
                    <li>
                        <!-- Second Tier Drop Down -->
                        <label for="drop-3" class="toggle"></label>
                        <a href="#">Férfi</a>
                        <input type="checkbox" id="drop-4"/>
                        <ul>
                            <li><a href="/csapatok/veszprem">TELEKOM VESZPRÉM</a></li>
                            <li><a href="/csapatok/szeged">MOL-PICK SZEGED</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li>
                <!-- First Tier Drop Down -->
                <label for="drop-5" class="toggle"><i class="fa fa-briefcase"></i> Versenyek</label>
                <a href="#"><i class="fa fa-briefcase"></i> Versenyek <i class="fa fa-caret-down"></i></a>
                <input type="checkbox" id="drop-5"/>
                <ul>
                    <li><a href="/europabajnoksag">Európa bajnokság</a></li>
                    <li><a href="/vilagbajnoksag">Világbajnokság</a></li>
                    <li><a href="/europaikupak">Európai Kupák</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</header>

@if (!(\Request::is('/')))
<section class="banner_area" style="background: url('{{ asset('/images/banner.jpg')}}') no-repeat fixed; background-position: center;" data-stellar-background-ratio="0.5">
    <h2>@yield('cim')</h2>
    <ol class="breadcrumb justify-content-center">
        <li class="breadcrumb-item"><a href="/">Kezdőlap</a></li>
        <li class="breadcrumb-item active" aria-current="page">@yield('menu')</li>
    </ol>
</section>
@endif

@yield('content')

<a id="fel"></a>

<footer>
    <footer class="footer-distributed">

        <div class="footer-right">

            <a href="#"><i class="fab fa-facebook"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-linkedin"></i></a>
            <a href="#"><i class="fab fa-github"></i></a>

        </div>

        <div class="footer-left">

            <p class="footer-links">
                <a href="#">Eredmények</a>
                ·
                <a href="">Tabella</a>
                ·
                <a href="">Fórum</a>
                ·
                <a href="">Hírek</a>
                ·
                <a href="#">Versenyek</a>
                ·
                <a href="#">Csapatok</a>
                ·
                <a href="#">Shop</a>
                ·
                <a href="#">Belépés</a>

            </p>

            <p>Handball World &copy; 2019</p>
        </div>

    </footer>
</footer>
@yield('js')
</body>











<script>
    var btn = $('#fel');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });

    $('.form').find('input, textarea').on('keyup blur focus', function (e) {

        var $this = $(this),
            label = $this.prev('label');

        if (e.type === 'keyup') {
            if ($this.val() === '') {
                label.removeClass('active highlight');
            } else {
                label.addClass('active highlight');
            }
        } else if (e.type === 'blur') {
            if( $this.val() === '' ) {
                label.removeClass('active highlight');
            } else {
                label.removeClass('highlight');
            }
        } else if (e.type === 'focus') {

            if( $this.val() === '' ) {
                label.removeClass('highlight');
            }
            else if( $this.val() !== '' ) {
                label.addClass('highlight');
            }
        }

    });

    $('.tab a').on('click', function (e) {

        e.preventDefault();

        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');

        target = $(this).attr('href');

        $('.tab-content > div').not(target).hide();

        $(target).fadeIn(600);

    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>


    <style type="text/css">
        .header {
            background: #8a8a8a;
        }
        .header .columns {
            padding-bottom: 0;
        }
        .header p {
            color: #fff;
            margin-bottom: 0;
        }
        .header .wrapper-inner {
            padding: 20px; /*controls the height of the header*/
        }
        .header .container {
            background: #8a8a8a;
        }
        .wrapper.secondary {
            background: #f3f3f3;
        }
    </style>
    <container>

        <spacer size="16"></spacer>

        <row>
            <columns>
                <h1>Üdv, {{ $order->billing_name }}</h1>
                <p class="lead">A HandballWorld oldaláról a következő megrendelés érkezett</p>
                <p>
                    A rendelés száma: {{ $order->id }}<br>
                    A rendelés dátuma: {{$order->created_at}}
                </p>
                <callout class="primary">
                    <h3>Rendelés részletei</h3>
                    <p>
                        Név: {{ $order->billing_name }}<br>
                        Cím: {{ $order->billing_postalcode }} {{ $order->billind_city }} {{ $order->billing_address }}<br>
                        Telefonszám: {{ $order->billing_phone }}<br>
                        Email: {{ $order->billing_email }}<br>
                        Fizetés: {{ $order->payment_gateway }}<br>
                    </p>
                    <h3>Megrendelt termékek</h3>
                    <p>
                        @foreach ($order->products as $product)
                            Termék megnevezés: {{ $product->name }} <br>
                            Darab/Ár: {{ round($product->price)}} HUF <br>
                            Mennyiség: {{ $product->pivot->quantity }} <br>
                        @endforeach
                        Végösszeg: {{ $order->billing_total}} HUF
                    </p>
                </callout>
                <p>
                    Köszönjük hogy minket választott!<br>
                    Tisztelettel,<br>
                    HandballWorld
                </p>
            </columns>
        </row>

        <wrapper class="secondary">
            <spacer size="16"></spacer>
            <row>
                <columns small="12" large="6">
                    <h5>Connect With Us:</h5>
                    <menu class="vertical">
                        <item style="text-align: left;" href="#">Twitter</item><br>
                        <item style="text-align: left;" href="#">Facebook</item><br>
                        <item style="text-align: left;" href="#">Google +</item>
                    </menu>
                </columns>
                <columns small="12" large="6">
                    <h5>Kapcsolatok:</h5>
                    <p>Phone: 000000000</p>
                    <p>Email: <a href="mailto:foundation@zurb.com">handballworld00@gmail.com</a></p>
                </columns>
            </row>
        </wrapper>

    </container>


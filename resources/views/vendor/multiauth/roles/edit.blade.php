@extends('multiauth::layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Rang szerkesztése</div>

                <div class="card-body">
                    <form action="{{ route('admin.role.update', $role->id) }}" method="post">
                        @csrf @method('patch')
                        <div class="form-group">
                            <label for="role">Rang eEve</label>
                            <input type="text" value="{{ $role->name }}" name="name" class="form-control" id="role">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Szerkesztés</button>
                        <a href="{{ route('admin.roles') }}" class="btn btn-danger btn-sm float-right">Vissza</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

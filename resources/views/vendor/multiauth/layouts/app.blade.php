<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('images/kezilogo.png') }}"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Handball World</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"  />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>

<body>
    <div id="app">
        <nav class="menu" tabindex="0">
            <div class="smartphone-menu-trigger"></div>
            <header class="avatar">
                <img src="{{ asset('images/kezilogozold.png') }}">
                <h2>Handball World</h2>
            </header>
            <ul>
                <a href="{{ route('admin.show') }}"><li tabindex="0" class="icon-dashboard"><span>Admin Lista</span></li></a>
                <a href="{{ route('admin.roles') }}"><li tabindex="0" class="icon-customers"><span>Rangok</span></li></a>
                <a href="{{ route('admin.password.change') }}"><li tabindex="0" class="icon-users"><span>Jelszó változtatás</span></li></a>
                <a href="{{route('admin.order')}}"><li tabindex="0" class="icon-order"><span>Rendelések</span></li></a>
                <a href="/Pages/gyori"><li tabindex="0" class="icon-gyor"><span>Győr</span></li></a>
                <a href="/Pages/fradii"><li tabindex="0" class="icon-fradi"><span>Fradi</span></li></a>
                <a href="/Pages/veszpremi"><li tabindex="0" class="icon-veszprem"><span>Veszprém</span></li></a>
                <a href="{{ route('admin.szeged') }}"><li tabindex="0" class="icon-szeged"><span>Szeged</span></li></a>
                <a href="{{ route('admin.siofok') }}"><li tabindex="0" class="icon-sio"><span>Siófok</span></li></a>
            </ul>
        </nav>


        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="#">Handball World</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        @guest('admin')
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('admin.login')}}">{{ ucfirst(config('multiauth.prefix')) }} Bejelentkezés</a>
                            </li>
                        @else
                            <li class="nav-item dropdown lenyilo">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false" v-pre>
                                    {{ auth('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/admin/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item loginki">
                                <a class="nav-link" style="float: right" href="/admin/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>

</html>
<!-- Authentication Links -->

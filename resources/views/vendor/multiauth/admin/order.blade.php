@extends('multiauth::layouts.app')
@section('content')



    <div class="container">



        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Shipped</th>
                <th>Not Shipped</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->billing_name }}</td>
                    <td>{{ $order->billing_email }}</td>
                    <td>
                        <input type="radio" data-id="{{ $order->id }}" id="chechbox-{{$order->id}}" name="status-{{$order->id}}" class="js-switch " {{ $order->shipped == 1 ? 'checked' : '' }}>

                    </td>
                    <td>
                        <input type="radio" data-id="{{ $order->id }}" id="chechbox-{{$order->id}}" name="status-{{$order->id}}" class="js-switch " {{ $order->shipped == 0 ? 'checked' : '' }}>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <script>

        $(document).ready(function(){
            $('.js-switch').change(function () {
                let shipped = ($(this).prop('checked') === true ? 1 : 0)&&($(this).prop('checked') === false ? 1 : 0);
                let id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '{{ route('order.update.status') }}',
                    data: {'id': id, 'shipped': shipped},
                    success: function (data) {
                        console.log(data.message);
                    }
                });
            });
        });

    </script>











@endsection



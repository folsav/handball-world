@extends('multiauth::layouts.app')
@section('content')


    <div class="container">
        <div class="row">
            <div class="table-responsive col-lg-12 col-sm-12 col-md-12">
                @include('flash-message')
                <a href="/szegedcreate" type="button" class="btn btw" style="margin-bottom: 15px">Új játékos</a>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Kép</th>
                        <th scope="col">Név</th>
                        <th scope="col">Poszt</th>
                        <th scope="col">Szám</th>
                        <th scope="col">Nemzet</th>
                        <th scope="col">Szerkesztés</th>
                    </tr>
                    </thead>
                    @php
                        $i=0;
                    @endphp
                    @foreach($Szeged as $szeged)

                        <tbody>
                        <tr>
                            <th scope="row"><?php $i++; echo $i; ?></th>
                            <td><img src="{{ asset('images/pick') }}/{{$szeged->kep}}" style="width: 150px; height: 200px"></td>
                            <td>{{$szeged->nev}}</td>
                            <td>{{$szeged->poszt}}</td>
                            <td>{{$szeged->mez}}</td>
                            <td>{{$szeged->nemzet}}</td>
                            <td>
                                <form action="{{route('szeged.torol', $szeged->id)}}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-danger" style="margin-bottom: 5px">Törlés</button>
                                </form>
                                <a href="/szegededit/{{$szeged->id}}" type="button" class="btn btw ">Szerksesztés</a>
                            </td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
            <div class="row">
                <div class="col-12 text-center d-flex justify-content-center">
                    {{$Szeged->links()}}
                </div>
            </div>
    </div>












@endsection



@extends('multiauth::layouts.app')
@section('content')

<div class="container">
    <div class="jumbotron">
        <h1>Játékos Adatainak Szerkesztése!</h1>
        <form action="/siofokedit/{{$siofok->id}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label>Név</label>
                <input type="text" name="nev" value="{{$siofok->nev}}" class="form-control" placeholder="Név">
            </div>
            <div class="form-group">
                <label>Poszt</label>
                <input type="text" name="poszt" value="{{$siofok->poszt}}" class="form-control" placeholder="Poszt">
            </div>
            <div class="form-group">
                <label>Mezszám</label>
                <input type="number" name="mez" value="{{$siofok->mez}}" class="form-control" placeholder="Mezszám">
            </div>
            <div class="form-group">
                <label>Nemzet</label>
                <input type="text" name="nemzet" value="{{$siofok->nemzet}}" class="form-control" placeholder="Nemzet">
            </div>
            <div class="form-group">
                <label>Kor</label>
                <input type="number" name="kor" value="{{$siofok->kor}}" class="form-control" placeholder="Kor">
            </div>
            <div class="form-group">
                <label>Magasság</label>
                <input type="number" name="magassag" value="{{$siofok->magassag}}" class="form-control" placeholder="Magasság">
            </div>
            <div class="form-group">
                <label>Születésiidő</label>
                <input type="date" name="szulido" value="{{$siofok->szulido}}" class="form-control" placeholder="Születésiidő">
            </div>
            <div class="form-group">
                <label>Születésihely</label>
                <input type="text" name="szulhey" value="{{$siofok->szulhey}}" class="form-control" placeholder="Születésihely">
            </div>
            <div class="input-group">
                <input type="file" name="kep" value="{{$siofok->kep}}" class="custom-file-input">
                <label class="custom-file-label">Kép</label>
            </div>
            <button type="submit" name="submit" class="btn btn-primary" style="margin-top: 15px">Save</button>
            <a href="/adminsiofok" type="submit" name="submit" class="btn btn-primary" style="float: right;display: inline-block; margin-top: 15px">Vissza</a>
        </form>

    </div>
</div>




















@endsection

@extends('multiauth::layouts.app')
@section('content')
    <div class="container">
        <div class="jumbotron">
        <h1>Játékos hozzáadása!</h1>
        <form action="{{ route('addsio') }}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label>Név</label>
                <input type="text" name="nev" class="form-control" placeholder="Név">
            </div>
            <div class="form-group">
                <label>Poszt</label>
                <input type="text" name="poszt" class="form-control" placeholder="Poszt">
            </div>
            <div class="form-group">
                <label>Mezszám</label>
                <input type="number" name="mez" class="form-control" placeholder="Mezszám">
            </div>
            <div class="form-group">
                <label>Nemzet</label>
                <input type="text" name="nemzet" class="form-control" placeholder="Nemzet">
            </div>
            <div class="form-group">
                <label>Kor</label>
                <input type="number" name="kor" class="form-control" placeholder="Kor">
            </div>
            <div class="form-group">
                <label>Magasság</label>
                <input type="number" name="magassag" class="form-control" placeholder="Magasság">
            </div>
            <div class="form-group">
                <label>Születésiidő</label>
                <input type="date" name="szulido" class="form-control" placeholder="Születésiidő">
            </div>
            <div class="form-group">
                <label>Születésihely</label>
                <input type="text" name="szulhey" class="form-control" placeholder="Születésihely">
            </div>
            <div class="input-group">
                <input type="file" name="kep" class="custom-file-input">
                <label class="custom-file-label">Kép</label>
            </div>
            <button type="submit" name="submit" class="btn btn-primary" style="margin-top: 15px">Save</button>
            <a href="/adminsiofok" type="submit" name="submit" class="btn btn-primary" style="float: right;display: inline-block; margin-top: 15px">Vissza</a>
        </form>
        </div>
    </div>




@endsection

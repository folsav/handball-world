@extends('multiauth::layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ ucfirst(config('multiauth.prefix')) }} Oldal</div>

                <div class="card-body">
                @include('multiauth::message')
                     Üdvözöllekl a HandballWorld Admin oldalán!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

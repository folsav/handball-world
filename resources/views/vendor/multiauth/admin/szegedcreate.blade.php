@extends('multiauth::layouts.app')
@section('content')
    <div class="container">
        <div class="jumbotron">
            <h1>Játékos hozzáadása!</h1>
            <form action="{{ route('addszeged') }}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Név</label>
                    <input type="text" name="nev" class="form-control" placeholder="Név">
                </div>
                <div class="form-group">
                    <label>Poszt</label>
                    <input type="text" name="poszt" class="form-control" placeholder="Poszt">
                </div>
                <div class="form-group">
                    <label>Mezszám</label>
                    <input type="number" name="mez" class="form-control" placeholder="Mezszám">
                </div>
                <div class="form-group">
                    <label>Nemzet</label>
                    <input type="text" name="nemzet" class="form-control" placeholder="Nemzet">
                </div>
                <div class="form-group">
                    <label>Kor</label>
                    <input type="number" name="kor" class="form-control" placeholder="Kor">
                </div>
                <div class="form-group">
                    <label>Magasság</label>
                    <input type="number" name="magassag" class="form-control" placeholder="Magasság">
                </div>
                <div class="form-group">
                    <label>Születésiidő</label>
                    <input type="date" name="szulido" class="form-control" placeholder="Születésiidő">
                </div>
                <div class="form-group">
                    <label>Születésihely</label>
                    <input type="text" name="szulhely" class="form-control" placeholder="Születésihely">
                </div>
                <div class="form-group">
                    <label>Csillagjegy</label>
                    <input type="text" name="Csillagjegy" class="form-control" placeholder="Csillagjegy">
                </div>
                <div class="form-group">
                    <label>Családiállapot</label>
                    <input type="text" name="csaladiallapot" class="form-control" placeholder="Családiállapot">
                </div>
                <div class="form-group">
                    <label>Előző klubbok</label>
                    <input type="text" name="elozoklubbok" class="form-control" placeholder="Előző klubbok">
                </div>
                <div class="form-group">
                    <label>Válogatottsag</label>
                    <input type="text" name="valogatottsag" class="form-control" placeholder="Válogatottsag">
                </div>
                <div class="form-group">
                    <label>Hobbi</label>
                    <input type="text" name="hobbi" class="form-control" placeholder="Hobbi">
                </div>
                <div class="form-group">
                    <label>Kedvenc ország</label>
                    <input type="text" name="orszag" class="form-control" placeholder="Kedvenc ország">
                </div>
                <div class="form-group">
                    <label>Kedvenc város</label>
                    <input type="text" name="varos" class="form-control" placeholder="Kedvenc Város">
                </div>
                <div class="form-group">
                    <label>Kedvenc Csapat</label>
                    <input type="text" name="csapat" class="form-control" placeholder="Kedvenc csapat">
                </div>
                <div class="form-group">
                    <label>Kedvenc játékos</label>
                    <input type="text" name="jatekos" class="form-control" placeholder="Kedvenc játékos">
                </div>
                <div class="form-group">
                    <label>Kedvenc Színész</label>
                    <input type="text" name="szinesz" class="form-control" placeholder="Kedvenc Színész">
                </div>
                <div class="form-group">
                    <label>Kedvenc Autó</label>
                    <input type="text" name="auto" class="form-control" placeholder="Kedvenc Autó">
                </div>
                <div class="input-group">
                    <label class="custom-file-label">Kép</label>
                    <input type="file" name="kep" class="custom-file-input">
                </div>
                <button type="submit" name="submit" class="btn btn-primary" style="margin-top: 15px">Save</button>
                <a href="/adminszeged" type="submit" name="submit" class="btn btn-primary" style="float: right;display: inline-block; margin-top: 15px">Vissza</a>
            </form>
        </div>
    </div>




@endsection

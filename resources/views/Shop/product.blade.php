@extends('shop.shoplayout')
@section('title', $product->name)
@section('menu')
    Web Shop
@endsection
@section('cim')
    Termékek
@endsection
@section('content')
    <div class="container">
        @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="product-section container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="product-section-image">
                    <img src="{{ asset('images/shop/') }}/{{$product->slug.'.jpg'}}" style="width: 100%" alt="product">
                </div>
            </div>
            <div class="product-section-information col-lg-6 col-md-6 col-sm-12">
                <h1 class="product-section-title">{{ $product->name }}</h1>
                <div class="product-section-subtitle">{{ $product->details }}</div>

                <div class="product-section-price">{{ $product->price }} HUF</div>
                <p>
                    {!! $product->description !!}
                </p>
                <p>&nbsp;</p>
                <form action="{{ route('cart.store') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$product->id}}">
                    <input type="hidden" name="name" value="{{$product->name}}">
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <button type="submit" class="button button-plain">Kosárba</button>
                </form>
            </div>
        </div>
    </div>



    <div class="might-like-section">
        <div class="container">
            <div class="row">
                @foreach ($mightAlsoLike as $product)
                    <div class="col-lg-3 col-md-6 col-sm-12 might-like-grid">
                    <a href="{{ route('shop.show', $product->id) }}" class="might-like-product">
                        <img src="{{ asset('images/shop/') }}/{{$product->slug.'.jpg'}}" style="width: 100%" alt="product">
                        <div class="might-like-product-name">{{ $product->name }}</div>
                        <div class="might-like-product-price">{{ $product->price }}</div>
                    </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


@endsection



@extends('shop.shoplayout')
@section('menu')
    Web Shop
@endsection
@section('cim')
    Web Shop
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <h1 class="my-4">Kategóriák</h1>
                <div class="list-group">
                    @foreach($categories as $category)
                        <a href="{{route('shop', ['category' => $category->slug])}}" class="list-group-item">{{$category->name}}</a>
                    @endforeach
                </div>

            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9">
                <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img class="d-block img-fluid" src="{{ asset('images/shop/eto.jpg') }}" style="width: 100%; height: 350px">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid" src="{{ asset('images/shop/pick.jpg') }}" style="width: 100%; height: 350px">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid" src="{{ asset('images/shop/veszprem.jpg') }}" style="width: 100%; height: 350px">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>


                <div class="row">

                    @foreach($porducts as $product)
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100 shopkepek">
                                <a href="{{{route('shop.show', $product->id)}}}"><img src="{{ asset('images/shop/') }}/{{$product->slug.'.jpg'}}" style="width: 100%;"></a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="{{{route('shop.show', $product->id)}}}">{{$product->name}}</a>
                                    </h4>
                                    <h5>{{$product->price}} HUF</h5>

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <!-- /.row -->

            </div>
            <!-- /.col-lg-9 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection

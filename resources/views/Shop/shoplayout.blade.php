<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title', 'Handball World')</title>
    <link rel="icon" type="image/png" href="{{ asset('images/kezilogo.png') }}"/>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    @yield('extra-css')
</head>


<body>
<div class="shop">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
        <div class="container">
            <div class="row">
                <a class="nav-link nava" href="/" style="font-size: 30px; color: white"><img src="{{ asset('images/kezilogozold.png') }}" alt="" style="width: 50px; padding: 0!important;" >
                    Handball World</a>
            <button class="navbar-toggler navbutyok" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    @guest
                    <li class="nav-item">
                        <a  class="nav-link" href="/login"><i class="fa fa-user"></i> Belépés</a>
                    </li>
                    @else
                        <li class="nav-item">
                            <a  class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i> Kilépés
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @endguest
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('shop')}}"><i class="fas fa-tshirt"></i> Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('cart.index')}}"><i class="fas fa-shopping-cart"></i> Kosár <span class="cart-count">
                            @if(Cart::instance('default')->count() > 0)
                                    <span>{{Cart::instance('default')->count()}}</span></span>
                            @endif
                        </a>
                    </li>
                </ul>
            </div>
            </div>
        </div>
    </nav>
</div>
@if (!(\Request::is('Pages/index')))
    <section class="banner_area" style="background: url('{{ asset('/images/banner.jpg')}}') no-repeat fixed; background-position: center;" data-stellar-background-ratio="0.5">
        <h2>@yield('cim')</h2>
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Kezdőlap</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('menu')</li>
        </ol>
    </section>
@endif

@yield('content')

<a id="fel"></a>

<footer>
    <footer class="footer-distributed">

        <div class="footer-right">

            <a href="#"><i class="fab fa-facebook"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-linkedin"></i></a>
            <a href="#"><i class="fab fa-github"></i></a>

        </div>

        <div class="footer-left">

            <p class="footer-links">
                <a href="#">Eredmények</a>
                ·
                <a href="/Pages/tabella">Tabella</a>
                ·
                <a href="/forums">Fórum</a>
                ·
                <a href="/Pages/hirek">Hírek</a>
                ·
                <a href="#">Versenyek</a>
                ·
                <a href="#">Csapatok</a>
                ·
                <a href="#">Shop</a>
                ·
                <a href="#">Belépés</a>

            </p>

            <p>Handball World &copy; 2019</p>
        </div>

    </footer>
</footer>
</body>

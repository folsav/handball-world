@extends('layout')
@section('title')
    Európai Kupák
@endsection
@section('menu')
    Európai Kupák
@endsection
@section('cim')
    Európai Kupák
@endsection

@section('content')

    <div class="container">
        <nav>
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                   aria-controls="nav-home" aria-selected="true">Férfi Bajnokokligája</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                   aria-controls="nav-profile" aria-selected="false">Női Bajnokokligája</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab"
                   aria-controls="nav-contact" aria-selected="false">Férfi EHF Kupa</a>
                <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab"
                   aria-controls="nav-about" aria-selected="false">Női EHF Kupa</a>
            </div>
        </nav>
        <section>
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane  show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        <div class="informacio col-lg-12">
                            <br>
                            <h1>Férfi Bajnokok Ligája:</h1>
                            <p>
                                Az EHF-bajnokok ligája a legjelentősebb európai kupasorozat a kézilabdában, amelyen
                                klubcsapatok vesznek részt. A versenyt minden évben az
                                Európai Kézilabda-szövetség írja ki. A Bajnokok Ligája küzdelemsorozatot először
                                1957-ben csak a férfi ágban rendezték meg. Ebben az évben
                                még nem is igazából a kézilabda klubok, hanem a városok versengtek egymással. 1959-től
                                rendezik kifejezetten a klubcsapatok részére ezt a versenyt,
                                amelyet a kezdetekkor még Bajnokcsapatok Európa Kupájának neveztek. Nők számára 1961-től
                                rendezik meg a viadalt.
                                Legtöbb győzelemmel a Barcelona csapata rendelkezik, amely kilenc alkalommal hódította
                                el a győztesnek járó trófeát.
                            </p>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="kupakep">
                                    <img onclick="changeimg(this)" src="{{ asset('images/ehf/ehfferfi1.jpg') }}" alt="">
                                    <img onclick="changeimg(this)" src="{{ asset('images/ehf/ehfferfi2.jpg') }}" alt="">
                                    <img onclick="changeimg(this)" src="{{ asset('images/ehf/ehfferfi3.jpg') }}" alt="">
                                    <img onclick="changeimg(this)" src="{{ asset('images/ehf/ehf.jpg') }}" alt="">
                                </div>
                                <div class="pre">
                                    <img id="p" src="{{ asset('images/ehf/ehf.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                            <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                            <h6>Ez a táblázat a 2011 óta rendezett bajnokokligája legjobb játékosait és gólkirályait
                                tartalmazza.</h6>
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Legjobb játékos</th>
                                    <th scope="col">Gólkirály</th>
                                    <th scope="col">Gólok száma</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl)
                                    @if ($elbl['nem'] == 'ferfi' and $elbl['verseny'] == 'bl')
                                        @if($elbl['golok'] > '0')
                                            <tr>
                                                <th scope="row">{{$elbl->ev}}</th>
                                                <td>{{$elbl->legjobbjatekos}}</td>
                                                <td>{{$elbl->golkiraly}}</td>
                                                <td>{{$elbl->golok}}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="table-responsive-xl col-lg-12">
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Aranyérmes</th>
                                    <th scope="col">Eredmény</th>
                                    <th scope="col">Ezüstérmes</th>
                                    <th scope="col">Bronzérmes</th>
                                    <th scope="col">Negyedik helyezett</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl) @if ($elbl['nem'] == 'ferfi' and $elbl['verseny'] == 'bl')
                                    <tr>
                                        <th scope="row">{{$elbl->ev}}</th>
                                        <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slug}}"
                                                 style="width: 25px; height: 25px"> {{$elbl->arany}}</td>
                                        <th>{{$elbl->eredmenyelso}}-{{$elbl->eredmenymasodik}}</th>
                                        <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->sluge}}"
                                                 style="width: 25px; height: 25px"> {{$elbl->ezust}}</td>
                                        <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugb}}"
                                                 style="width: 25px; height: 25px"> {{$elbl->bronz}}</td>
                                        <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugn}}"
                                                 style="width: 25px; height: 25px"> {{$elbl->negyedik}}</td>
                                    </tr>
                                @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="row">
                        <div class="informacio col-lg-12">
                            <br>
                            <h1>Női Bajnokok Ligája:</h1>
                            <p>
                                A női EHF-bajnokok ligája a legjelentősebb európai kupasorozat a női kézilabdában,
                                amelyen klubcsapatok vesznek részt. A versenyt minden évben
                                az Európai Kézilabda-szövetség írja ki. A bajnokcsapatok Európa-kupája küzdelemsorozatot
                                először 1957-ben csak a férfi ágban rendezték meg.
                                Nők számára 1961-től rendezik meg a viadalt. 1993 óta a sorozat neve a női EHF-bajnokok
                                ligája (EHF Women’s Champions League) nevet viseli.
                                A döntőket 2013-ig oda-visszavágós rendszerben játszották. Az összesítésben több gólt
                                szerzett együttes nyerte a döntőt. 2014 óta az elődöntőket,
                                a bronzmérkőzést és a döntőt egy előre kiválasztott helyszínen rendezik, és egyenes
                                kieséses rendszerben (Final Four) egy mérkőzésen dől el a
                                döntőbe jutás, valamint a győztes kiléte.
                            </p>
                        </div>
                        <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                            <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                            <h6>Ez a táblázat a 2014 óta rendezett bajnokokligája legjobb játékosait és gólkirályait
                                tartalmazza.</h6>
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Legjobb játékos</th>
                                    <th scope="col">Gólkirály</th>
                                    <th scope="col">Gólok száma</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl)
                                    @if ($elbl['nem'] == 'noi' and $elbl['verseny'] == 'bl')
                                        @if($elbl['golok'] > '0')
                                            <tr>
                                                <th scope="row">{{$elbl->ev}}</th>
                                                <td>{{$elbl->legjobbjatekos}}</td>
                                                <td>{{$elbl->golkiraly}}</td>
                                                <td>{{$elbl->golok}}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="kupakep">
                                    <img onclick="changeimg2(this)" src="{{ asset('images/ehf/ehfnoi1.jpg') }}" alt="">
                                    <img onclick="changeimg2(this)" src="{{ asset('images/ehf/ehfnoi2.jpg') }}" alt="">
                                    <img onclick="changeimg2(this)" src="{{ asset('images/ehf/ehfnoi3.jpg') }}" alt="">
                                    <img onclick="changeimg2(this)" src="{{ asset('images/ehf/ehfnoi.jpg') }}" alt="">
                                </div>
                                <div class="pre">
                                    <img id="b" src="{{ asset('images/ehf/ehfnoi.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive-xl col-lg-12">
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Aranyérmes</th>
                                    <th scope="col">Eredmény</th>
                                    <th scope="col">Ezüstérmes</th>
                                    <th scope="col">Bronzérmes</th>
                                    <th scope="col">Negyedik helyezett</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl)
                                    @if ($elbl['nem'] == 'noi' and $elbl['verseny'] == 'bl')
                                        <tr>
                                            <th scope="row">{{$elbl->ev}}</th>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slug}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->arany}}</td>
                                            <th>{{$elbl->eredmenyelso}}-{{$elbl->eredmenymasodik}}</th>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->sluge}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->ezust}}</td>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugb}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->bronz}}</td>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugn}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->negyedik}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <div class="row">
                        <div class="informacio col-lg-12">
                            <br>
                            <h1>Férfi EHF Kupa:</h1>
                            <p>
                                Az EHF-kupa egy, az Európai Kézilabda-szövetség (EHF) által klubcsapatok számára évente
                                megrendezett nemzetközi kézilabdaverseny.
                                A kupa célja, hogy azok a csapatok, amelyeknek nem sikerült bejutniuk a Bajnokok
                                Ligájába egy külön megmérettetésen részt vehessenek.
                                Éppen ezért ezt a kupasorozatot tartják a harmadik számú nemzetközi bajnokságnak - a
                                Bajnokok Ligája, és a Kupagyőztesek Európa Kupája mögött.
                                2013-tól a KEK megszűnt, így az EHF kupa lett a 2. számú kupa a BL mögött. A kupa
                                lebonyolítását eredetileg a Nemzetközi Kézilabda-szövetség (IHF)
                                intézte 1982-től és eszerint a kupa elnevezése is IHF-kupa volt, később, 1993-tól vette
                                át ezen feladatokat az EHF, és ekkor kapta a jelenlegi nevét is.
                                A kupában a versengés végig az egyenes kiesés szabályai szerint zajlik. Minden
                                fordulóban összepárosítják a csapatokat, amelyek oda-visszavágón eldöntik a
                                továbbjutás kérdését. Az egyes mérkőzésein szerzett gólok összeadódnak, és az
                                összesítésben több gólt szerző csapat juthat be a következő fordulóba. Amennyiben
                                gólegyenlőség alakulna ki, úgy az a csapat kerül tovább, amelyik idegenben a több gólt
                                szerezte. Ha így sem lehet dönteni, akkor a visszavágó után közvetlenül
                                hétméteresekkel döntenek. 2012-től megváltoztatták a kupa kiírását. 2013-től final
                                four-os lebonyolítással rendezték a döntőket.
                            </p>
                        </div>
                        <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="kupakep">
                                    <img onclick="changeimg3(this)" src="{{ asset('images/ehf/ehfk1.jpg') }}" alt="">
                                    <img onclick="changeimg3(this)" src="{{ asset('images/ehf/ehfk2.jpg') }}" alt="">
                                    <img onclick="changeimg3(this)" src="{{ asset('images/ehf/ehfk3.jpg') }}" alt="">
                                    <img onclick="changeimg3(this)" src="{{ asset('images/ehf/ehfk.jpg') }}" alt="">
                                </div>
                                <div class="pre">
                                    <img id="a" src="{{ asset('images/ehf/ehfk.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                            <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                            <h6>Ez a táblázat a 2009 óta rendezett világbajnokság legjobb játékosait és gólkirályait
                                tartalmazza.</h6>
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Legjobb játékos</th>
                                    <th scope="col">Gólkirály</th>
                                    <th scope="col">Gólok száma</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl)
                                    @if ($elbl['nem'] == 'ferfi' and $elbl['verseny'] == 'el')
                                        @if($elbl['golok'] > '0')
                                            <tr>
                                                <th scope="row">{{$elbl->ev}}</th>
                                                <td>{{$elbl->legjobbjatekos}}</td>
                                                <td>{{$elbl->golkiraly}}</td>
                                                <td>{{$elbl->golok}}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="table-responsive-xl col-lg-12">
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Aranyérmes</th>
                                    <th scope="col">Eredmény</th>
                                    <th scope="col">Ezüstérmes</th>
                                    <th scope="col">Bronzérmes</th>
                                    <th scope="col">Negyedik helyezett</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl)
                                    @if ($elbl['nem'] == 'ferfi' and $elbl['verseny'] == 'el')
                                        <tr>
                                            <th scope="row">{{$elbl->ev}}</th>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slug}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->arany}}</td>
                                            <th>{{$elbl->eredmenyelso}}-{{$elbl->eredmenymasodik}}</th>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->sluge}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->ezust}}</td>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugb}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->bronz}}</td>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugn}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->negyedik}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                    <div class="row">
                        <div class="informacio col-lg-12">
                            <br>
                            <h1>Női EHF Kupa:</h1>
                            <p>
                                Az EHF-kupa egy, az Európai Kézilabda-szövetség (EHF) által klubcsapatok számára évente
                                megrendezett nemzetközi kézilabdaverseny.
                                A kupa célja, hogy azok a csapatok, amelyeknek nem sikerült bejutniuk a Bajnokok
                                Ligájába egy külön megmérettetésen részt vehessenek.
                                Éppen ezért ezt a kupasorozatot tartják a harmadik számú nemzetközi bajnokságnak - a
                                Bajnokok Ligája, és a Kupagyőztesek Európa Kupája mögött.
                                2013-tól a KEK megszűnt, így az EHF kupa lett a 2. számú kupa a BL mögött. A kupa
                                lebonyolítását eredetileg a Nemzetközi Kézilabda-szövetség (IHF)
                                intézte 1982-től és eszerint a kupa elnevezése is IHF-kupa volt, később, 1993-tól vette
                                át ezen feladatokat az EHF, és ekkor kapta a jelenlegi nevét is.
                                A kupában a versengés végig az egyenes kiesés szabályai szerint zajlik. Minden
                                fordulóban összepárosítják a csapatokat, amelyek oda-visszavágón eldöntik a
                                továbbjutás kérdését. Az egyes mérkőzésein szerzett gólok összeadódnak, és az
                                összesítésben több gólt szerző csapat juthat be a következő fordulóba. Amennyiben
                                gólegyenlőség alakulna ki, úgy az a csapat kerül tovább, amelyik idegenben a több gólt
                                szerezte. Ha így sem lehet dönteni, akkor a visszavágó után közvetlenül
                                hétméteresekkel döntenek. 2012-től megváltoztatták a kupa kiírását.
                            </p>
                        </div>
                        <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                            <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                            <h6>Ez a táblázat a 2009 óta rendezett világbajnokság legjobb játékosait és gólkirályait
                                tartalmazza.</h6>
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Legjobb játékos</th>
                                    <th scope="col">Gólkirály</th>
                                    <th scope="col">Gólok száma</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl)
                                    @if ($elbl['nem'] == 'noi' and $elbl['verseny'] == 'el')
                                        @if($elbl['golok'] > '0')
                                            <tr>
                                                <th scope="row">{{$elbl->ev}}</th>
                                                <td>{{$elbl->legjobbjatekos}}</td>
                                                <td>{{$elbl->golkiraly}}</td>
                                                <td>{{$elbl->golok}}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                            <div class="row">
                                <div class="kupakep">
                                    <img onclick="changeimg4(this)" src="{{ asset('images/ehf/ehfkn1.jpg') }}" alt="">
                                    <img onclick="changeimg4(this)" src="{{ asset('images/ehf/ehfkn2.jpg') }}" alt="">
                                    <img onclick="changeimg4(this)" src="{{ asset('images/ehf/ehfkn3.jpg') }}" alt="">
                                    <img onclick="changeimg4(this)" src="{{ asset('images/ehf/ehfkn.png') }}" alt="">
                                </div>
                                <div class="pre">
                                    <img id="c" src="{{ asset('images/ehf/ehfkn.png') }}" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive-xl col-lg-12">
                            <table class="table table-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Év</th>
                                    <th scope="col">Aranyérmes</th>
                                    <th scope="col">Eredmény</th>
                                    <th scope="col">Ezüstérmes</th>
                                    <th scope="col">Bronzérmes</th>
                                    <th scope="col">Negyedik helyezett</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Elbl as $elbl)
                                    @if ($elbl['nem'] == 'noi' and $elbl['verseny'] == 'el')
                                        <tr>
                                            <th scope="row">{{$elbl->ev}}</th>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slug}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->arany}}</td>
                                            <th>{{$elbl->eredmenyelso}}-{{$elbl->eredmenymasodik}}</th>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->sluge}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->ezust}}</td>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugb}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->bronz}}</td>
                                            <td><img src="{{ asset('images/csapatlogok') }}/{{$elbl->slugn}}"
                                                     style="width: 25px; height: 25px"> {{$elbl->negyedik}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        function changeimg(image) {
            var p = document.getElementById("p");
            p.src = image.src;
            p.parentElement.style.display = "block";
        }

        function changeimg2(image) {
            var b = document.getElementById("b");
            b.src = image.src;
            b.parentElement.style.display = "block";
        }

        function changeimg3(image) {
            var a = document.getElementById("a");
            a.src = image.src;
            a.parentElement.style.display = "block";
        }

        function changeimg4(image) {
            var c = document.getElementById("c");
            c.src = image.src;
            c.parentElement.style.display = "block";
        }
    </script>
@endsection

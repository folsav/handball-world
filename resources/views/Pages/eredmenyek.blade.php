@extends('layout')
@section('title')
    Eredmények
@endsection
@section('menu')
    Eredmények
@endsection
@section('cim')
    Eredmények
@endsection
@section('content')

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-12 col-sm-12 px-0">
            <div class="d-flex align-items-center justify-content-end">
                <i class="calendar-logo fas fa-calendar-day mr-2"></i>
                <input type="text" class="calendar" id="datepicker" value="{{$date}}">
            </div>
        </div>
    </div>
    @foreach($meccsek as $key => $meccs)
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 col-sm-12 tabella-bg mb-2 d-flex align-items-center">
                <div class="p-1 d-flex align-items-center">
                    <img src="{{ asset('images/zaszlok/eng') }}/{{$meccs['country']['slug']}}" style="width: 30px; height: 18px" class="mr-2">
                    <p class="mb-0">
                        {{$meccs['country']['country']}} : {{$key}}
                    </p>
                </div>
            </div>
        </div>
        @foreach($meccs['list'] as $k =>  $merkozos)
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-3 d-flex align-items-center">
                            <p class="">{{$merkozos['status']}}</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-5 d-flex align-items-center">
                            <div class="d-block">
                                <p class="<?php if($merkozos['0']['home_score'] > $merkozos['1']['away_score']){echo 'font-weight-bold'; } ?>">
                                    {{$merkozos['0']['home']}}
                                </p>
                                <p class="<?php if($merkozos['0']['home_score'] < $merkozos['1']['away_score']){echo 'font-weight-bold'; } ?>">
                                    {{$merkozos['1']['away']}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-4">
                            <div class="row">
                                <div class="col-4">
                                    <div class="d-block">
                                        <p class="font-weight-bold">
                                            {{$merkozos['0']['home_score']}}
                                        </p>
                                        <p class="font-weight-bold">
                                            {{$merkozos['1']['away_score']}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="d-block">
                                        <p class="modal-time">
                                            @if(isset($merkozos['0']['home_score_period_first']))
                                                {{$merkozos['0']['home_score_period_first']}}
                                            @endif
                                        </p>
                                        <p class="modal-time">
                                            @if(isset($merkozos['1']['away_score_period_first']))
                                                {{$merkozos['1']['away_score_period_first']}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="d-block">
                                        <p class="modal-time">
                                            @if(isset($merkozos['0']['home_score_period_second']))
                                                {{$merkozos['0']['home_score_period_second']}}
                                            @endif
                                        </p>
                                        <p class="modal-time">
                                            @if(isset($merkozos['1']['away_score_period_second']))
                                                {{$merkozos['1']['away_score_period_second']}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 align-items-center justify-content-end d-none d-sm-none d-md-flex d-lg-flex">
                            <button type="button" class="btn tabella-btn" data-toggle="modal" data-target="#exampleModal-{{$k}}">Részletek</button>
                        </div>
                    </div>
                    @if(!$loop->last)
                        <hr class="my-0 mb-2">
                    @endif
                </div>
            </div>

            <div class="modal fade" id="exampleModal-{{$k}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header justify-content-center border-0">
                            <div class="row w-100 justify-content-center align-items-center">
                                <div class="col-12">
                                    <img src="{{ asset('images/zaszlok/eng') }}/{{$meccs['country']['slug']}}" style="width: 30px; height: 18px" class="mr-2">
                                    <p>{{$meccs['country']['country']}} : {{$key}}</p>
                                </div>
                                <div class="col-12 d-flex justify-content-center align-items-center">
                                    <p class="modal-time">{{$merkozos['start_time']}}</p>
                                </div>
                                <div class="col-4 d-flex justify-content-center align-items-center flex-column">
                                    <p class="tabella-modal-team">
                                        {{$merkozos['0']['home']}}
                                    </p>
                                </div>
                                <div class="col-4 d-flex justify-content-center align-items-center">
                                    <p class="tabella-modal-eredmeny">{{$merkozos['0']['home_score']}} - {{$merkozos['1']['away_score']}}</p>
                                </div>
                                <div class="col-4 d-flex justify-content-center align-items-center flex-column">
                                    <p class="tabella-modal-team">
                                        {{$merkozos['1']['away']}}
                                    </p>
                                </div>
                                <div class="col-12 d-flex justify-content-center align-items-center">
                                    {{$merkozos['status']}}
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="row mx-0">
                                <div class="col-12 d-flex align-items-center justify-content-center modal-border mb-4">
                                    <h5>Eredmények</h5>
                                </div>
                                <div class="col-6">
                                    <div class="d-block">
                                        <p class="<?php if($merkozos['0']['home_score'] > $merkozos['1']['away_score']){echo 'font-weight-bold'; } ?>">
                                            {{$merkozos['0']['home']}}
                                        </p>
                                        <p class="<?php if($merkozos['0']['home_score'] < $merkozos['1']['away_score']){echo 'font-weight-bold'; } ?>">
                                            {{$merkozos['1']['away']}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="d-block">
                                        <p class="font-weight-bold">
                                            {{$merkozos['0']['home_score']}}
                                        </p>
                                        <p class="font-weight-bold">
                                            {{$merkozos['1']['away_score']}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="d-block">
                                        <p class="modal-time">
                                            @if(isset($merkozos['0']['home_score_period_first']))
                                                {{$merkozos['0']['home_score_period_first']}}
                                            @endif
                                        </p>
                                        <p class="modal-time">
                                            @if(isset($merkozos['1']['away_score_period_first']))
                                                {{$merkozos['1']['away_score_period_first']}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="d-block">
                                        <p class="modal-time">
                                            @if(isset($merkozos['0']['home_score_period_second']))
                                                {{$merkozos['0']['home_score_period_second']}}
                                            @endif
                                        </p>
                                        <p class="modal-time">
                                            @if(isset($merkozos['1']['away_score_period_second']))
                                                {{$merkozos['1']['away_score_period_second']}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row w-100 mx-0 justify-content-center">
                                <div class="col-12 d-flex align-items-center justify-content-center modal-border mb-2">
                                    <h5>Részletek</h5>
                                </div>
                                <div class="col-12">
                                    <div class="d-flex justify-content-between">
                                        <p>
                                            Helyszín:
                                        </p>
                                        <p>
                                            {{$merkozos['city_name']}}
                                        </p>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <p>
                                            Aréna:
                                        </p>
                                        <p>
                                            {{$merkozos['arena_name']}}
                                        </p>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <p>
                                            Csarnok kapacitása:
                                        </p>
                                        <p>
                                            {{$merkozos['capacity']}} Fő
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Bezár</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endforeach
</div>

    <script>


        /* Hungarian initialisation for the jQuery UI date picker plugin. */
        ( function( factory ) {
            if ( typeof define === "function" && define.amd ) {

                // AMD. Register as an anonymous module.
                define( [ "../widgets/datepicker" ], factory );
            } else {

                // Browser globals
                factory( jQuery.datepicker );
            }
        }( function( datepicker ) {

            datepicker.regional.hu = {
                closeText: "Bezár",
                prevText: "Vissza",
                nextText: "Előre",
                currentText: "Ma",
                monthNames: [ "Január", "Február", "Március", "Április", "Május", "Június",
                    "Július", "Augusztus", "Szeptember", "Október", "November", "December" ],
                monthNamesShort: [ "Jan", "Feb", "Már", "Ápr", "Máj", "Jún",
                    "Júl", "Aug", "Szep", "Okt", "Nov", "Dec" ],
                dayNames: [ "Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat" ],
                dayNamesShort: [ "Vas", "Hét", "Ked", "Sze", "Csü", "Pén", "Szo" ],
                dayNamesMin: [ "V", "H", "K", "Sze", "Cs", "P", "Szo" ],
                weekHeader: "Hét",
                dateFormat: "yy.mm.dd.",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: true,
                yearSuffix: "" };
            datepicker.setDefaults( datepicker.regional.hu );

            return datepicker.regional.hu;

        } ) );


        $( function() {
            $( "#datepicker" ).datepicker({
                "dateFormat": "yy-mm-dd",
                "minDate": -7,
                "maxDate": +7,
            }).on("change", function() {
                var selected = $(this).val();
                window.location.href = "/eredmenyek/"+selected;
            });
        } );


    </script>


@endsection



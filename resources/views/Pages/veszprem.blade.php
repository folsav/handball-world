@extends('layout')
@section('title')
    Veszprém
@endsection
@section('menu')
    Csapatok
@endsection
@section('cim')
    TELEKOM Veszprém
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="bevezeto">
                <h1>Játékosok:<img id="logo" src="{{ asset('images/veszprem/veszpremlogo.png') }}"></h1>
            </div>
        </div>
    </div>

    <div class="row">
        @php $i=0; @endphp
        @foreach($Veszprem as $veszprem)
            <div class="content col-lg-3 col-md-6 col-sm-12">
                <div class="card middle">
                    <div class="front">
                        <img src="{{ asset('images/veszprem') }}/{{$veszprem["img"]}}">
                    </div>
                    <div class="back">
                        <div class="back-content middle">
                            <div class="sm">
                                <p> <span class="alma">Név:</span> {{$veszprem["name"]}}<br>
                                    <span class="alma">Poszt:</span> {{$veszprem["type"]}}<br>
                                    <span class="alma">Állampolgárság:</span> {{$veszprem["nationality"]}}<br>
                                    <span class="alma">Kor:</span> {{$veszprem["kor"]}}<br>
                                    <span class="alma">Magasság:</span> {{$veszprem["height"]}} cm<br>
                                    <span class="alma">Születési idő:</span> {{$veszprem["date_of_birth"]}}<br>
                                    <span class="alma">Születési hely:</span> {{$veszprem["szuletesihely"]}}<br>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <h3><a href="" data-toggle="modal" data-id="{{$veszprem["id"]}}" data-target="#exampleModal-{{$i}}">{{$veszprem["name"]}}</a></h3>
            </div>
            @php $i++; @endphp
        @endforeach
    </div>
        <div class="row">
            <div class="col-6">
                <div class="szakmaistab">
                    <h1>Szakmai stáb:</h1>
                    <p><span class="alma">Vezetőedző:</span> David Davis<br>
                        <span class="alma">Másodedző:</span> Carlos Reinaldo Perez<br>
                        <span class="alma">Másodedző:</span> Porobic Haris<br>
                        <span class="alma">Orvos:</span> Dr. Sydó Tibor<br>
                        <span class="alma">Orvos:</span> Dr. Mahunka Zsolt<br>
                        <span class="alma">Gyúró:</span> Végh József<br>
                        <span class="alma">Gyúró:</span> Nemanja Vucic<br>
                        <span class="alma">Gyúró:</span> Szücs Antal<br>
                    </p>
                </div>
            </div>
            <div class="col-6">
                <div class="sikerek">
                    <h1>Elért eredmények:</h1>
                    <p><span class="alma">Magyar bajnoki arany:</span> 25 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar Superkupa győztes:</span> 3 <i class="fas fa-trophy"></i><br>
                        <span class="alma">KEK győztes:</span> 2 <i class="fas fa-trophy"></i><br>
                        <span class="alma">KEK döntős:</span> 2 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar kupa arany:</span> 27 <i class="fas fa-trophy"></i><br>
                        <span class="alma">EHF bajnokokligája döntős:</span> 3 <i class="fas fa-trophy"></i><br>
                        <span class="alma">EHF FINAL4:</span> 4 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Sehaliga győztes:</span> 2 <i class="fas fa-trophy"></i><br>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="altinfo">
                    <h1>A klubbról röviden:<img id="logo" src="{{ asset('images/veszprem/veszpremlogo.png') }}"></h1>
                    <p>
                        A Telekom Veszprém KC 25-szörös magyar bajnok, 27-szeres magyarkupa-győztes, 2-szeres EHF-kupagyőztesek Európa-kupája győztes és 3-szoros EHF Bajnokok Ligája döntős veszprémi férfi kézilabdacsapat.
                        Magyarország legsikeresebb kézilabda egyesülete.
                    </p>
                    <p>
                        Veszprémben régi hagyományai voltak a kézilabdasportnak, a Bakony Vegyész TC női gárdája 1970-ben a vidéki együttesek közül elsőként nyert bajnoki címet.
                        A sportegyesület a Veszprém Megyei Állami Építőipari Vállalat (VÁÉV) szárnyai alatt Veszprémi Építők néven alakult meg 1977-ben, miután a városban politikai döntés született,
                        amely azt szorgalmazta, hogy az NB II-ből kiesett BVTC férfi szakosztályát átvegye a VÁÉV. Az új szakosztály Hajnal Csaba ügyvezető irányítása alatt a megyei bajnokságból indulva
                        1981-ben feljutott az első osztályba, ahol egyedülálló módon minden szezont éremmel zárt; rögtön az első szezonban ezüstérmes lett. A következő három évben,
                        immáron VÁÉV-Építők néven a bajnokságban egy ezüst- és két bronzérmet, a kupában két ezüst- és egy aranyérmet nyertek. 1985-ben és 1986-ban bajnok lett a csapat.
                        A következő négy évben a csapat csupán négy ezüstéremig jutott (három alkalommal a Rába ETO, egyszer az Elektromos mögött), ezután viszont páratlan sikerszériába kezdett az 1990-től
                        1992-ig Bramac, 2005-ig Fotex, 2015-ig MKB, azóta pedig az MVM által támogatott csapat: 1992 óta, 23 szezon alatt 20 bajnoki arany- és 3 ezüstérem került Veszprémbe.
                        (Eközben 2008. május és 2011. október között egyetlen bajnokit sem vesztettek el.)
                        A kupában az 1984-es siker után szintén 3 győri elsőség következett, majd 1988-tól kezdve 24 év alatt 19 kupagyőzelem gyarapítja a dicsőséglistát,
                        aminek a legfényesebb eredménye négy KEK-döntő (2 győzelem és 2 ezüstérem), valamint két EHF-bajnokok ligája 2. helyezés.
                        2008. júliusa óta a Veszprém Aréna jelenti az MKB Veszprém KC számára a hazai pályát, korábban a Március 15. utcai csarnokban játszották a mérkőzéseiket.
                </div>
            </div>
        </div>
    </div>
@endsection

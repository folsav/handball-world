@extends('layout')
@section('title')
    Európabajnokság
@endsection
@section('menu')
    Európabajnokságok
@endsection
@section('cim')
    Európa-bajnokságok
@endsection

@section('content')



    <div class="container">
        <nav>
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Férfi Eb</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Női Eb</a>
            </div>
        </nav>
    <section>
    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
        <div class="tab-pane  show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="row">
                    <div class="informacio col-lg-12">
                        <br>
                        <h1>Férfi Kézilabda-Európa bajnokság:</h1>
                        <p>
                            A férfi kézilabda-Európa-bajnokság az Európai Kézilabda-szövetség szervezésében, minden páros évben, kétévente megrendezésre kerülő nemzetközi kézilabdatorna.
                            Az első Eb-t 1994-ben rendezték, hasonlóan a női Európa-bajnokságokhoz. A férfi Eb-ket jellemzően januárban rendezik meg,
                            ahol a selejtezőket követően 16 nemzet válogatottja vesz részt.
                            A 2020-as Európa-bajnokságon már 24 csapat fog részt venni. Az Eb-t Ausztria, Norvégia és Svédország közösen rendezi.
                            A férfiaknál a svéd együttes idáig négyszer volt Európa-bajnok. 2010-ben második alkalommal nyert a francia csapat. Magyarország még nem szerzett érmet a kontinenstornán.
                        </p>
                    </div>
                    <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                        <h1>Éremtáblázat:<i class="fas fa-medal float-right"></i></h1>
                        <h6>Ez táblázat az 1994–2018-ig megrendezett férfi Európa-bajnokságokon érmet nyert csapatokat tartalmazza.</h6>
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Helyezés</th>
                                <th scope="col">Nemzet</th>
                                <th scope="col">Arany</th>
                                <th scope="col">Ezüst</th>
                                <th scope="col">Bronz</th>
                                <th scope="col">Összesen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Eremtablaeb as $eremtablaeb)
                                @if ($eremtablaeb['nem'] == 'Férfi')
                                    <tr>
                                        <th scope="row">{{$eremtablaeb->helyezes}}</th>
                                        <td><img src="{{ asset('images/zaszlok') }}/{{$eremtablaeb->slug}}" style="width: 30px; height: 20px"> {{$eremtablaeb->nemzet}}</td>
                                        <td>{{$eremtablaeb->arany}}</td>
                                        <td>{{$eremtablaeb->ezust}}</td>
                                        <td>{{$eremtablaeb->bronz}}</td>
                                        <td>{{$eremtablaeb->arany+$eremtablaeb->ezust+$eremtablaeb->bronz}}</td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                        <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                        <h6>Ez a táblázat a 2008 óta rendezett európabajnokság legjobb játékosait és gólkirályait tartalmazza.</h6>
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Év</th>
                                <th scope="col">Legjobb játékos</th>
                                <th scope="col">Gólkirály</th>
                                <th scope="col">Gólok száma</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Eb as $eb)
                                @if ($eb['nem'] == 'Férfi')
                                    @if($eb['golok'] > '0')
                                        <tr>
                                            <th scope="row">{{$eb->vbev}}</th>
                                            <td>{{$eb->legjobbjatekos}}</td>
                                            <td>{{$eb->golkiraly}}</td>
                                            <td>{{$eb->golok}}</td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive-xl col-lg-12">
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Év</th>
                                <th scope="col">Rendező</th>
                                <th scope="col">Aranyérmes</th>
                                <th scope="col">Eredmény</th>
                                <th scope="col">Ezüstérmes</th>
                                <th scope="col">Bronzérmes</th>
                                <th scope="col">Eredmény</th>
                                <th scope="col">Negyedik helyezett</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Eb as $eb) @if ($eb['nem'] == 'Férfi')
                                <tr>
                                    <th scope="row">{{$eb->vbev}}</th>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->slug}}" style="width: 30px; height: 20px"> {{$eb->rendezo}}</td>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->sluga}}" style="width: 30px; height: 20px"> {{$eb->arany}}</td>
                                    <th>{{$eb->eredmenyelso}}-{{$eb->eredmenymasodik}}</th>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->sluge}}" style="width: 30px; height: 20px"> {{$eb->ezust}}</td>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->slugb}}" style="width: 30px; height: 20px"> {{$eb->bronz}}</td>
                                    <th>{{$eb->eredmenyharmadik}}–{{$eb->eredmenynegyedik}}</th>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->slugn}}" style="width: 30px; height: 20px"> {{$eb->negyedik}}</td>
                                </tr>
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
        <div class="tab-pane" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="row">
                    <div class="informacio col-lg-12">
                        <br>
                        <h1>Női Kézilabda-Európa bajnokság:</h1>
                        <p>
                            A női kézilabda-Európa-bajnokság az Európai Kézilabda-szövetség szervezésében, minden páros évben, kétévente negrendezésre kerülő
                            nemzetközi kézilabdatorna. Az első Eb-t 1994-ben rendezték, hasonlóan a férfi Európa-bajnokságokhoz. A női Eb-ket jellemzően decemberben rendezik meg,
                            ahol a selejtezőket követően 16 nemzet válogatottja vesz részt.
                            A magyar női kézilabda-válogatott 2000-ben, Romániában szerzett Európa-bajnoki címet. Három alkalommal bronzérmes lett.
                            A 2018-as Európa-bajnokságot Franciaországban rendezte, a 2020-asnak közösen Norvégia és Dánia ad otthont.
                        </p>
                    </div>
                    <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                        <h1>Éremtáblázat:<i class="fas fa-medal float-right"></i></h1>
                        <h6>Az alábbi táblázat az 1994–2018-ig megrendezett Európa-bajnokságokon érmet nyert csapatokat tartalmazza.</h6>
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Helyezés</th>
                                <th scope="col">Nemzet</th>
                                <th scope="col">Arany</th>
                                <th scope="col">Ezüst</th>
                                <th scope="col">Bronz</th>
                                <th scope="col">Összesen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Eremtablaeb as $eremtablaeb)
                                @if ($eremtablaeb['nem'] == 'Női')
                                    <tr>
                                        <th scope="row">{{$eremtablaeb->helyezes}}</th>
                                        <td><img src="{{ asset('images/zaszlok') }}/{{$eremtablaeb->slug}}" style="width: 30px; height: 20px"> {{$eremtablaeb->nemzet}}</td>
                                        <td>{{$eremtablaeb->arany}}</td>
                                        <td>{{$eremtablaeb->ezust}}</td>
                                        <td>{{$eremtablaeb->bronz}}</td>
                                        <td>{{$eremtablaeb->arany+$eremtablaeb->ezust+$eremtablaeb->bronz}}</td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                        <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                        <h6>Ez a táblázat a 2012 óta rendezett európabajnokság legjobb játékosait és gólkirályait tartalmazza.</h6>
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Év</th>
                                <th scope="col">Legjobb játékos</th>
                                <th scope="col">Gólkirály</th>
                                <th scope="col">Gólok száma</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Eb as $eb)
                                @if ($eb['nem'] == 'Női')
                                    @if($eb['golok'] > '0')
                                        <tr>
                                            <th scope="row">{{$eb->vbev}}</th>
                                            <td>{{$eb->legjobbjatekos}}</td>
                                            <td>{{$eb->golkiraly}}</td>
                                            <td>{{$eb->golok}}</td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive-xl col-lg-12">
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Év</th>
                                <th scope="col">Rendező</th>
                                <th scope="col">Aranyérmes</th>
                                <th scope="col">Eredmény</th>
                                <th scope="col">Ezüstérmes</th>
                                <th scope="col">Bronzérmes</th>
                                <th scope="col">Eredmény</th>
                                <th scope="col">Negyedik helyezett</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Eb as $eb) @if ($eb['nem'] == 'Női')
                                <tr>
                                    <th scope="row">{{$eb->vbev}}</th>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->slug}}" style="width: 30px; height: 20px"> {{$eb->rendezo}}</td>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->sluga}}" style="width: 30px; height: 20px"> {{$eb->arany}}</td>
                                    <th>{{$eb->eredmenyelso}}-{{$eb->eredmenymasodik}}</th>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->sluge}}" style="width: 30px; height: 20px"> {{$eb->ezust}}</td>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->slugb}}" style="width: 30px; height: 20px"> {{$eb->bronz}}</td>
                                    <th>{{$eb->eredmenyharmadik}}–{{$eb->eredmenynegyedik}}</th>
                                    <td><img src="{{ asset('images/zaszlok') }}/{{$eb->slugn}}" style="width: 30px; height: 20px"> {{$eb->negyedik}}</td>
                                </tr>
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
        </section>
    </div>
@endsection

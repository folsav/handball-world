@extends('layout')
@section('title')
Fradi
@endsection
@section('menu')
    Csapatok
@endsection
@section('cim')
    FTC-Rail Cargo Hungaria
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="bevezeto">
                <h1>Játékosok:<img id="logo" src="{{ asset('images/fradi/logo.png') }}"></h1>
            </div>
        </div>
    </div>

    <div class="row">
        @php $i=0; @endphp
        @foreach($Fradi as $fradi)
            <div class="content col-lg-3 col-md-6 col-sm-12">
                <div class="card middle">
                    <div class="front">
                        <img src="{{ asset('images/fradi') }}/{{$fradi["img"]}}">
                    </div>
                    <div class="back">
                        <div class="back-content middle">
                            <div class="sm">
                                <p> <span class="alma">Név:</span> {{$fradi["name"]}}<br>
                                    <span class="alma">Poszt:</span> {{$fradi["type"]}}<br>
                                    <span class="alma">Állampolgárság:</span> {{$fradi["nationality"]}}<br>
                                    <span class="alma">Kor:</span> {{$fradi["kor"]}}<br>
                                    <span class="alma">Magasság:</span> {{$fradi["height"]}} cm<br>
                                    <span class="alma">Születési idő:</span> {{$fradi["date_of_birth"]}}<br>
                                    <span class="alma">Születési hely:</span> {{$fradi["szuletesihely"]}}<br>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <h3><a href="" data-toggle="modal" data-id="{{$fradi["id"]}}" data-target="#exampleModal-{{$i}}">{{$fradi["name"]}}</a></h3>
            </div>
            @php $i++; @endphp
        @endforeach
    </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="szakmaistab">
                    <h1>Szakmai stáb:</h1>
                    <p><span class="alma">Vezetőedző:</span> Elek Gábor<br>
                        <span class="alma">Másodedző:</span> Kovács Attila<br>
                        <span class="alma">Gyógytornász:</span> Vajay-Gazsó Dorottya<br>
                        <span class="alma">Masszőr</span> Pais Péter<br>
                        <span class="alma">Orvos:</span> Dr. Pavlik Attila<br>
                        <span class="alma">Kapusedző:</span> Duleba Norbert<br>
                        <span class="alma">Erőnléti edző:</span> Tord Ellingsen<br>
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="sikerek">
                    <h1>Elért eredmények:</h1>
                    <p><span class="alma">Magyar bajnoki arany:</span> 12 <i class="fas fa-trophy"></i><br>
                        <span class="alma">KEK győztes:</span> 3 <i class="fas fa-trophy"></i><br>
                        <span class="alma">EHF kupa győztes:</span> 1 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar kupa arany:</span> 12 <i class="fas fa-trophy"></i><br>
                        <span class="alma">EHF bajnokokligája elődöntős:</span> 3 <i class="fas fa-trophy"></i><br>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="altinfo">
                    <h1>A klubbról röviden:<img id="logo" src="{{ asset('images/fradi/logo.png') }}"></h1>
                    <p>
                        Az FTC-Rail Cargo Hungaria egy magyar női kézilabdacsapat, többszörös magyar bajnok, kupagyőztes, KEK győztes, mely nemzetközi szinten is az élvonalba tartozik.
                    </p>
                    <p>
                        A Ferencvárosi TC női kézilabda szakosztályát 1950-ben alapították, és politikai utasításra akkor még az ÉDOSZ nevet viselte a klub többi szakosztályához hasonlóan,
                        melyet később újabb rendelettel Budapesti Kinizsire változtattak. Kezdetben kis- és nagypályán egyaránt zajlottak a mérkőzések, ám a Fradi az ötvenes években
                        elsősorban a nagypályás játékra összpontosított, melyben jó bajnoki helyezéseket ért el (második: 1957; harmadik: 1955, 1958). Az elsőség megszerzése ugyan nem
                        sikerült, de a Magyar Népköztársasági Kupa (MNK) 1955-ös győztese a Ferencváros lett. Az akkori keretből Bende Mária és Dévényi Mária válogatott lett.
                    </p>
                    <p>
                        2008-tól a korábbi sikeredző, Elek Gyula fia, Elek Gábor vette át a csapat irányítását. A megfiatalított csapat első sikere a 2008-09-es évad ezüstérme,
                        illetve a 2010-es kupa-ezüstje lett. Ezt újabb dobogós helyezés követte a bajnokságban, ugyanis 2010-11-es szezonban 3. lett a Ferencváros. A 2014-2015-ös
                        idényben Magyar Kupa döntős, és az AUDI Győri-ETO együttesét a rájátszásban kétszer legyőzve magyar bajnoki címet szerzett a csapat Elek Gábor vezetésével.
                        A 2016-2017-es szezon kezdete előtt csatlakozott a csapathoz a Gőyri ETO-t elhagyó Kovacsics Anikó, a válogatott kapus Bíró Blanka és a világbajnoki ezüstérmes
                        holland Danick Snelder.A Magyar Kupa kecskeméti döntőjében 2017. április 2-án nagy küzdelemben, hétméteres párbajt követően legyőzték a Győri ETO-t és
                        tizennégy év után újra kupagyőzelmet ünnepelhettek. A jelenlegi keret több válogatott játékossal is büszkélkedhet: Schatzl Nadine, Lukács Viktória,
                        Háfra Noémi, Szucsánszki Zita, Nerea Pena, Bíró Blanka, Kovacsics Anikó és Danick Snelder.

                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection


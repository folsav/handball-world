@extends('layout')
@section('title')
    Tabellák
@endsection
@section('menu')
    Tabellák
@endsection
@section('cim')
    Tabellák
@endsection

@section('content')

    <div class="container">
        <div class="row" style="padding-top: 50px;">
            <div class="table-responsive col-lg-6 col-sm-12 col-md-12">
                <h3>{{$nb1['standings'][0]['nevminta']}} <img src="{{ asset('images') }}/{{$nb1["standings"][0]['bajnoksag']}}" class="logotabella"></h3>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tabella" role="tab" aria-controls="home" aria-selected="true">Tabella</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#forma" role="tab" aria-controls="profile" aria-selected="false">Forma</a>
                    </li>
                </ul>
                 <div class="tab-content">
                    <div class="tab-pane show active" id="tabella" role="tabpanel" aria-labelledby="home-tab">
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Csapatok</th>
                                <th scope="col">LM</th>
                                <th scope="col">GY</th>
                                <th scope="col">D</th>
                                <th scope="col">V</th>
                                <th scope="col">G</th>
                                <th scope="col">P</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($nb1['standings'][0]['groups'][0]['standings'] as $nbitabella)
                                <tr>
                                    <th scope="row">{{$nbitabella["rank"]}}</th>
                                    <td><img src="{{ asset('images/csapatlogok') }}/{{$nbitabella["competitor"]['img']}}" class="tabellalogo">&nbsp&nbsp{{$nbitabella["competitor"]['name']}}</td>
                                    <td>{{$nbitabella["played"]}}</td>
                                    <td>{{$nbitabella["win"]}}</td>
                                    <td>{{$nbitabella["draw"]}}</td>
                                    <td>{{$nbitabella["loss"]}}</td>
                                    <td>{{$nbitabella["goals_for"]}}-{{$nbitabella["goals_against"]}}</td>
                                    <th>{{$nbitabella["points"]}}</th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                     <div class="tab-pane show" id="alma" role="tabpanel" aria-labelledby="home-tab">
                         <ul class="nav nav-tabs" id="myTab" role="tablist">
                             <li class="nav-item">
                                 <a class="nav-link active" id="home-tab" data-toggle="tab" href="#forma" role="tab" aria-controls="home" aria-selected="true">13</a>
                             </li>
                             <li class="nav-item">
                                 <a class="nav-link" id="profile-tab" data-toggle="tab" href="#alma" role="tab" aria-controls="profile" aria-selected="false">26</a>
                             </li>
                         </ul>
                         <table class="table table-sm">
                             <thead class="thead-dark">
                             <tr>
                                 <th scope="col">#</th>
                                 <th scope="col">Csapatok</th>
                                 <th scope="col">LM</th>
                                 <th scope="col">GY</th>
                                 <th scope="col">D</th>
                                 <th scope="col">V</th>
                                 <th scope="col">G</th>
                                 <th scope="col">P</th>
                             </tr>
                             </thead>
                             <tbody>
                             @foreach($nb1['standings'][0]['groups'][0]['standings'] as $nbitabella)
                                 <tr>
                                     <th scope="row">{{$nbitabella["rank"]}}</th>
                                     <td><img src="{{ asset('images/csapatlogok') }}/{{$nbitabella["competitor"]['img']}}" class="tabellalogo">&nbsp&nbsp{{$nbitabella["competitor"]['name']}}</td>
                                     <td>{{$nbitabella["played"]}}</td>
                                     <td>{{$nbitabella["win"]}}</td>
                                     <td>{{$nbitabella["draw"]}}</td>
                                     <td>{{$nbitabella["loss"]}}</td>
                                     <td>{{$nbitabella["goals_for"]}}-{{$nbitabella["goals_against"]}}</td>
                                     <th>{{$nbitabella["points"]}}</th>
                                 </tr>
                             @endforeach
                             </tbody>
                         </table>
                     </div>
                    <div class="tab-pane" id="forma" role="tabpanel" aria-labelledby="profile-tab">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#forma" role="tab" aria-controls="home" aria-selected="true">13</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#alma" role="tab" aria-controls="profile" aria-selected="false">26</a>
                            </li>
                        </ul>
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Csapatok</th>
                                <th scope="col">LM</th>
                                <th scope="col">GY</th>
                                <th scope="col">D</th>
                                <th scope="col">V</th>
                                <th scope="col">G</th>
                                <th scope="col">P</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($nb1['standings'][1]['groups'][0]['standings'] as $nbitabella)
                                <tr>
                                    <th scope="row">{{$nbitabella["rank"]}}</th>
                                    <td><img src="{{ asset('images/csapatlogok') }}/{{$nbitabella["competitor"]['img']}}" class="tabellalogo">&nbsp&nbsp{{$nbitabella["competitor"]['name']}}</td>
                                    <td>{{$nbitabella["played"]}}</td>
                                    <td>{{$nbitabella["win"]}}</td>
                                    <td>{{$nbitabella["draw"]}}</td>
                                    <td>{{$nbitabella["loss"]}}</td>
                                    <td>{{$nbitabella["goals_for"]}}-{{$nbitabella["goals_against"]}}</td>
                                    <th>{{$nbitabella["points"]}}</th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="table-responsive col-lg-6 col-sm-12 col-md-12">
                <h3>{{$nemet['standings'][0]['nevminta']}} <img src="{{ asset('images') }}/{{$nemet["standings"][0]['bajnoksag']}}" class="logotabella"></h3>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tabellaa" role="tab" aria-controls="home" aria-selected="true">Tabella</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#formaa" role="tab" aria-controls="profile" aria-selected="false">Forma</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane show active" id="tabellaa" role="tabpanel" aria-labelledby="home-tab">
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Csapatok</th>
                                <th scope="col">LM</th>
                                <th scope="col">GY</th>
                                <th scope="col">D</th>
                                <th scope="col">V</th>
                                <th scope="col">G</th>
                                <th scope="col">P</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($nemet['standings'][0]['groups'][0]['standings'] as $bundestabella)
                                <tr>
                                    <th scope="row">{{$bundestabella["rank"]}}</th>
                                    <td><img src="{{ asset('images/csapatlogok') }}/{{$bundestabella["competitor"]['img']}}" class="tabellalogo">&nbsp&nbsp{{$bundestabella["competitor"]['name']}}</td>
                                    <td>{{$bundestabella["played"]}}</td>
                                    <td>{{$bundestabella["win"]}}</td>
                                    <td>{{$bundestabella["draw"]}}</td>
                                    <td>{{$bundestabella["loss"]}}</td>
                                    <td>{{$bundestabella["goals_for"]}}-{{$bundestabella["goals_against"]}}</td>
                                    <th>{{$bundestabella["points"]}}</th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane show" id="almaa" role="tabpanel" aria-labelledby="home-tab">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#formaa" role="tab" aria-controls="home" aria-selected="true">17</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#almaa" role="tab" aria-controls="profile" aria-selected="false">34</a>
                            </li>
                        </ul>
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Csapatok</th>
                                <th scope="col">LM</th>
                                <th scope="col">GY</th>
                                <th scope="col">D</th>
                                <th scope="col">V</th>
                                <th scope="col">G</th>
                                <th scope="col">P</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($nemet['standings'][0]['groups'][0]['standings'] as $bundestabella)
                                <tr>
                                    <th scope="row">{{$bundestabella["rank"]}}</th>
                                    <td><img src="{{ asset('images/csapatlogok') }}/{{$bundestabella["competitor"]['img']}}" class="tabellalogo">&nbsp&nbsp{{$bundestabella["competitor"]['name']}}</td>
                                    <td>{{$bundestabella["played"]}}</td>
                                    <td>{{$bundestabella["win"]}}</td>
                                    <td>{{$bundestabella["draw"]}}</td>
                                    <td>{{$bundestabella["loss"]}}</td>
                                    <td>{{$bundestabella["goals_for"]}}-{{$bundestabella["goals_against"]}}</td>
                                    <th>{{$bundestabella["points"]}}</th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="formaa" role="tabpanel" aria-labelledby="profile-tab">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#formaa" role="tab" aria-controls="home" aria-selected="true">17</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#almaa" role="tab" aria-controls="profile" aria-selected="false">34</a>
                            </li>
                        </ul>
                        <table class="table table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Csapatok</th>
                                <th scope="col">LM</th>
                                <th scope="col">GY</th>
                                <th scope="col">D</th>
                                <th scope="col">V</th>
                                <th scope="col">G</th>
                                <th scope="col">P</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($nemet['standings'][1]['groups'][0]['standings'] as $bundestabella)
                                <tr>
                                    <th scope="row">{{$bundestabella["rank"]}}</th>
                                    <td><img src="{{ asset('images/csapatlogok') }}/{{$bundestabella["competitor"]['img']}}" class="tabellalogo">&nbsp&nbsp{{$bundestabella["competitor"]['name']}}</td>
                                    <td>{{$bundestabella["played"]}}</td>
                                    <td>{{$bundestabella["win"]}}</td>
                                    <td>{{$bundestabella["draw"]}}</td>
                                    <td>{{$bundestabella["loss"]}}</td>
                                    <td>{{$bundestabella["goals_for"]}}-{{$bundestabella["goals_against"]}}</td>
                                    <th>{{$bundestabella["points"]}}</th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#myTab li:first-child a').tab('show')
        })
    </script>
@endsection







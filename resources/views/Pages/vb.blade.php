@extends('layout')
@section('title')
    Világbajnokság
@endsection
@section('menu')
    Világbajnokságok
@endsection
@section('cim')
    Világ-bajnokságok
@endsection

@section('content')


    <div class="container">
             <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Férfi Vb</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Női Vb</a>
                </div>
             </nav>
                <section>
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane  show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                            <div class="row">
                            <div class="informacio col-lg-12">
                                <br>
                                <h1>Férfi kézilabda-világbajnokság:</h1>
                                <p>
                                    A férfi kézilabda-világbajnokság a Nemzetközi Kézilabda-szövetség (International Handball Federation, IHF) szervezésében, minden páratlan évben,
                                    kétévente megrendezésre kerülő nemzetközi kézilabdatorna. A vb-n a selejtezők után 32 nemzet válogatottja vesz részt. A tornát jellemzően januárban rendezik.
                                    A 2021-es világbajnokságot Egyiptomban rendezik.
                                    Férfiak számára először 1938-ban írták ki a tornát, akkor még különválasztották a kézilabdának a teremben űzött, és a szabadtéren űzött változatát,
                                    és ezért mindkét irányág számára külön írták ki a világversenyt. A második világháború után nem azonos időközönként rendezték meg egyik tornát sem,
                                    általában három-négy év telt el két világbajnokság között. 1993 óta rendezik rendszeresen kétévente a világbajnokságokat. Az utolsó szabadtéri
                                    kézilabda-világbajnokság a férfiaknál 1966-ban került megrendezésre.
                                </p>
                            </div>
                            <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                                <h1>Éremtáblázat:<i class="fas fa-medal float-right"></i></h1>
                                <h6>Ez táblázat az 1938–2019 között megrendezett terem világbajnokságokon érmet nyert csapatokat tartalmazza.</h6>
                                <table class="table table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Helyezés</th>
                                        <th scope="col">Nemzet</th>
                                        <th scope="col">Arany</th>
                                        <th scope="col">Ezüst</th>
                                        <th scope="col">Bronz</th>
                                        <th scope="col">Összesen</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Eremtabla as $eremtabla)
                                        @if ($eremtabla['nem'] == 'Férfi')
                                            <tr>
                                                <th scope="row">{{$eremtabla->helyezes}}</th>
                                                <td><img src="{{ asset('images/zaszlok') }}/{{$eremtabla->slug}}" style="width: 30px; height: 20px"> {{$eremtabla->nemzet}}</td>
                                                <td>{{$eremtabla->arany}}</td>
                                                <td>{{$eremtabla->ezust}}</td>
                                                <td>{{$eremtabla->bronz}}</td>
                                                <td>{{$eremtabla->arany+$eremtabla->ezust+$eremtabla->bronz}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                                <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                                <h6>Ez a táblázat a 2009 óta rendezett világbajnokság legjobb játékosait és gólkirályait tartalmazza.</h6>
                                <table class="table table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Év</th>
                                        <th scope="col">Legjobb játékos</th>
                                        <th scope="col">Gólkirály</th>
                                        <th scope="col">Gólok száma</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Vb as $vb)
                                        @if ($vb['nem'] == 'Férfi')
                                            @if($vb['golok'] > '0')
                                                <tr>
                                                    <th scope="row">{{$vb->ebev}}</th>
                                                    <td>{{$vb->legjobbjatekos}}</td>
                                                    <td>{{$vb->golkiraly}}</td>
                                                    <td>{{$vb->golok}}</td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive-xl col-lg-12">
                                <table class="table table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Év</th>
                                        <th scope="col">Rendező</th>
                                        <th scope="col">Aranyérmes</th>
                                        <th scope="col">Eredmény</th>
                                        <th scope="col">Ezüstérmes</th>
                                        <th scope="col">Bronzérmes</th>
                                        <th scope="col">Eredmény</th>
                                        <th scope="col">Negyedik helyezett</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Vb as $vb)
                                        @if ($vb['nem'] == 'Férfi')
                                        <tr>
                                            <th scope="row">{{$vb->ebev}}</th>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->slug}}" style="width: 30px; height: 20px"> {{$vb->rendezo}}</td>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->sluga}}" style="width: 30px; height: 20px"> {{$vb->arany}}</td>
                                            <th>{{$vb->eredmenyelso}}-{{$vb->eredmenymasodik}}</th>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->sluge}}" style="width: 30px; height: 20px"> {{$vb->ezust}}</td>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->slugb}}" style="width: 30px; height: 20px"> {{$vb->bronz}}</td>
                                            <th>{{$vb->eredmenyharmadik}}–{{$vb->eredmenynegyedik}}</th>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->slugn}}" style="width: 30px; height: 20px"> {{$vb->negyedik}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                </div>
                <div class="tab-pane" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                        <div class="row">
                            <div class="informacio col-lg-12">
                                <br>
                                <h1>Női kézilabda-világbajnokság:</h1>
                                <p>
                                    A női kézilabda-világbajnokság az Nemzetközi Kézilabda-szövetség (International Handball Federation, IHF) szervezésében, minden páratlan évben,
                                    kétévente megrendezésre kerülő nemzetközi kézilabdatorna. A vb-n a selejtezők után 24 nemzet válogatottja vesz részt. A tornát jellemzően
                                    decemberben rendezik.
                                    A nőknél 1949–1960 között szabadtéri világbajnokságok voltak, teremben 1957-től rendezik meg a tornát, 1993 óta kétévente.
                                    A 2015-ös világbajnokságot Dánia,[1] a 2017-est Németország rendezte.
                                    A magyar női kézilabda-válogatott 1965-ben nyert vb-t. 1982-ben és Ausztriával közösen 1995-ben rendezője volt az eseménynek, mindkét esetben
                                    ezüstérmes lett.
                                </p>
                            </div>
                            <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                                <h1>Éremtáblázat:<i class="fas fa-medal float-right"></i></h1>
                                <h6> Ez táblázat az 1938–2019 között megrendezett terem világbajnokságokon érmet nyert csapatokat tartalmazza.</h6>
                                <table class="table table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Helyezés</th>
                                        <th scope="col">Nemzet</th>
                                        <th scope="col">Arany</th>
                                        <th scope="col">Ezüst</th>
                                        <th scope="col">Bronz</th>
                                        <th scope="col">Összesen</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Eremtabla as $eremtabla)
                                        @if ($eremtabla['nem'] == 'Női')
                                            <tr>
                                                <th scope="row">{{$eremtabla->helyezes}}</th>
                                                <td><img src="{{ asset('images/zaszlok') }}/{{$eremtabla->slug}}" style="width: 30px; height: 20px"> {{$eremtabla->nemzet}}</td>
                                                <td>{{$eremtabla->arany}}</td>
                                                <td>{{$eremtabla->ezust}}</td>
                                                <td>{{$eremtabla->bronz}}</td>
                                                <td>{{$eremtabla->arany+$eremtabla->ezust+$eremtabla->bronz}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive col-lg-6 col-md-12 col-sm-12">
                                <h1>Legjobb játékosok és gólkirályok:<i class="fas fa-trophy float-right"></i></h1>
                                <h6>Ez a táblázat a 2009 óta rendezett világbajnokság legjobb játékosait és gólkirályait tartalmazza.</h6>
                                <table class="table table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Év</th>
                                        <th scope="col">Legjobb játékos</th>
                                        <th scope="col">Gólkirály</th>
                                        <th scope="col">Gólok száma</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Vb as $vb)
                                        @if ($vb['nem'] == 'Női')
                                            @if($vb['golok'] > '0')
                                                <tr>
                                                    <th scope="row">{{$vb->ebev}}</th>
                                                    <td>{{$vb->legjobbjatekos}}</td>
                                                    <td>{{$vb->golkiraly}}</td>
                                                    <td>{{$vb->golok}}</td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive-xl col-lg-12">
                                <table class="table table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Év</th>
                                        <th scope="col">Rendező</th>
                                        <th scope="col">Aranyérmes</th>
                                        <th scope="col">Eredmény</th>
                                        <th scope="col">Ezüstérmes</th>
                                        <th scope="col">Bronzérmes</th>
                                        <th scope="col">Eredmény</th>
                                        <th scope="col">Negyedik helyezett</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Vb as $vb)
                                        @if ($vb['nem'] == 'Női')
                                        <tr>
                                            <th scope="row">{{$vb->ebev}}</th>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->slug}}" style="width: 30px; height: 20px"> {{$vb->rendezo}}</td>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->sluga}}" style="width: 30px; height: 20px"> {{$vb->arany}}</td>
                                            <th>{{$vb->eredmenyelso}}-{{$vb->eredmenymasodik}}</th>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->sluge}}" style="width: 30px; height: 20px"> {{$vb->ezust}}</td>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->slugb}}" style="width: 30px; height: 20px"> {{$vb->bronz}}</td>
                                            <th>{{$vb->eredmenyharmadik}}–{{$vb->eredmenynegyedik}}</th>
                                            <td><img src="{{ asset('images/zaszlok') }}/{{$vb->slugn}}" style="width: 30px; height: 20px"> {{$vb->negyedik}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
                </div>
                </section>
    </div>
@endsection


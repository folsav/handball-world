@extends('layout')
@section('title')
    Győr
@endsection
@section('menu')
    Csapatok
@endsection
@section('cim')
    Győr Audi ETO KC
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="bevezeto">
                <h1>Játékosok:<img id="logo" src="{{ asset('images/gyor/logo.png') }}"></h1>
            </div>
        </div>
    </div>

    <div class="row">
        @php $i=0; @endphp
        @foreach($Gyor as $gyor)
            <div class="content col-lg-3 col-md-6 col-sm-12">
                <div class="card middle">
                    <div class="front">
                        <img src="{{ asset('images/gyor') }}/{{$gyor["img"]}}">
                    </div>
                    <div class="back">
                        <div class="back-content middle">
                            <div class="sm">
                                <p> <span class="alma">Név:</span> {{$gyor["name"]}}<br>
                                    <span class="alma">Poszt:</span> {{$gyor["type"]}}<br>
                                    <span class="alma">Állampolgárság:</span> {{$gyor["nationality"]}}<br>
                                    <span class="alma">Kor:</span> {{$gyor["kor"]}}<br>
                                    <span class="alma">Magasság:</span> {{$gyor["height"]}} cm<br>
                                    <span class="alma">Születési idő:</span> {{$gyor["date_of_birth"]}}<br>
                                    <span class="alma">Születési hely:</span> {{$gyor["szuletesihely"]}} <br>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <h3><a href="" data-toggle="modal" data-id="{{$gyor["id"]}}" data-target="#exampleModal-{{$i}}">{{$gyor["name"]}}</a></h3>
            </div>
            @php $i++; @endphp
        @endforeach
    </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="szakmaistab">
                    <h1>Szakmai stáb:</h1>
                    <p><span class="alma">Vezetőedző:</span> Danyi Gábor<br>
                        <span class="alma">Másodedző:</span> Zdravko zovko<br>
                        <span class="alma">Videóelemző:</span> Kun Attila<br>
                        <span class="alma">Erőnléti edző:</span> Holanek Zoltán<br>
                        <span class="alma">Orvos:</span> Dr. Balogh Péter<br>
                        <span class="alma">Orvos:</span> Dr. Szálasy László<br>
                        <span class="alma">Masszőr:</span> Molnár Tamás<br>
                        <span class="alma">Masszőr:</span> Devecseri Ádám<br>
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="sikerek">
                    <h1>Elért eredmények:</h1>
                    <p><span class="alma">Magyar bajnoki arany:</span> 14 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar Superkupa győztes:</span> 2 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Bajnokok ligája győztes:</span> 4 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar kupa arany:</span> 14 <i class="fas fa-trophy"></i><br>
                        <span class="alma">EHF bajnokokligája döntős:</span> 3 <i class="fas fa-trophy"></i><br>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="altinfo">
                    <h1>A klubbról röviden:<img id="logo" src="{{ asset('images/gyor/logo.png') }}"></h1>
                    <p>
                        Győri Audi ETO KC Győr városának négyszeres Bajnokok Ligája győztes női kézilabdacsapata. Az utóbbi évek legsikeresebb magyar női klubcsapata, mind a hazai bajnokságot,
                        mind a nemzetközi szereplést tekintve. 2014-ben, 2017-ben és 2018-ban az év csapata lett Magyarországon
                    </p>
                    <p>
                        Az 1904-ben alapított ETO 1948-ban felkérte Sebők Tibort a női kézilabda-szakosztály megszervezésére. 1950-ben első helyen zárták a még nem hivatalos amatőr bajnokságot.
                        1951-től induló hivatalos magyar bajnokság területi csoportjába kerültek, amit 1957-ben sikerült megnyerniük, és ezzel feljutottak az NBI-be. Az első NBI-es évadjuk tavaszi fordulóját
                        a 6. helyen zárták (akkoriban tavaszi-őszi fordulók voltak), majd az őszi fordulóban egy bravúros hajrával sikerült megnyerniük a bajnokságot.
                        Itt még Győri Graboplast ETO 1962-től kezdve elkerülte a csapatot a szerencse, és 1966-ra a 13. helyig csúsztak vissza, ami a csapat kiesését jelentette.
                        1968-ban egy szezon elejéig újra feljutott az NBI-be. A mélyrepülés folytatódott tovább, 1977-ben kiestek az NBI/B-ből, majd 1979-ben az NBII-ből is. Az NBII-től való búcsúzás a női
                        szakosztály megszüntetéséhez vezetett.
                    </p>
                    <p>
                        A klub az elmúlt 19 évben mindig a női bajnokság dobogóján végzett, 2005, 2006 után 2008-tól zsinórban kilencszer nyert magyar bajnokságot, összesen 13-szor. 2017-ig
                        hússzor bejutott a Magyar Kupa négyes döntőjébe, 12-szer meg is nyerte. Tizenhatszor játszott minimum elődöntőt valamelyik nemzetközi kupában, ebből tizenegyszer döntőt, melyből a
                        2013-as és 2014-es Bajnokok Ligája döntőt meg is nyerte. 2017-ben pedig megszerezte harmadik Bajnokok Ligája címét a Vardar Szkopje ellen.
                        A következő idényben a hazai sorozatokban a Magyar Kupát az Érd NK ellen nyerték meg 27–23-ra, új rekordot jelentő tizenharmadik alkalommal. A bajnokok Ligája-sorozatban újra
                        döntőbe jutott a csapat, ahol újra a macedón Vardar Szkopje volt az ellenfél. Ezúttal is nagy küzdelmet, 20–20-as döntetlent és hosszabbítást követően 27–26-os győzelemmel sikerült
                        megszerezni a negyedik BL-sikert. A torna történetében, amióta a Final Four-rendszert bevezették, első csapatként védte meg a címet.

                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection


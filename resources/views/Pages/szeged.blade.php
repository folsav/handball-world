@extends('layout')
@section('title')
    Szeged
@endsection
@section('menu')
    Csapatok
@endsection
@section('cim')
    MOL-Pick Szeged
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="bevezeto">
                <h1>Játékosok:<img id="logo" src="{{ asset('images/pick/picklogo.png') }}"></h1>
            </div>
        </div>
    </div>

    <div class="row">
        @php $i=0; @endphp
        @foreach($Szeged as $szeged)
            <div class="content col-lg-3 col-md-6 col-sm-12">
                <div class="card middle">
                    <div class="front">
                        <img src="{{ asset('images/pick') }}/{{$szeged->kep}}">
                    </div>
                    <div class="back">
                        <div class="back-content middle">
                            <div class="sm">
                                <p> <span class="alma">Név:</span> {{$szeged->nev}}<br>
                                    <span class="alma">Poszt:</span> {{$szeged->poszt}}<br>
                                    <span class="alma">Állampolgárság:</span> {{$szeged->nemzet}}<br>
                                    <span class="alma">Kor:</span> {{$szeged->kor}}<br>
                                    <span class="alma">Magasság:</span> {{$szeged->magassag}} cm<br>
                                    <span class="alma">Születési idő:</span> {{$szeged->szulido}}<br>
                                    <span class="alma">Születési hely:</span> {{$szeged->szulhely}}<br>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <h3><a href="" data-toggle="modal" data-id="{{$szeged->id}}" data-target="#exampleModal-{{$i}}">{{$szeged->nev}}</a></h3>
            </div>

            <div class="modal" id="exampleModal-{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{$szeged->nev}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-5">
                                        <img class="d-block w-100" src="{{ asset('images/pick') }}/{{$szeged->kep}}">
                                </div>
                                <div class="col-lg-7">
                                    <p><span class="alma">Poszt:</span><span class="modalinfo"> {{$szeged->poszt}}</span><br>
                                        <span class="alma">Állampolgárság:</span><span class="modalinfo"> {{$szeged->nemzet}}</span><br>
                                        <span class="alma">Kor:</span><span class="modalinfo"> {{$szeged->kor}}</span><br>
                                        <span class="alma">Magasság:</span> <span class="modalinfo">{{$szeged->magassag}} cm</span><br>
                                        <span class="alma">Születési idő:</span><span class="modalinfo"> {{$szeged->szulido}}</span><br>
                                        <span class="alma">Születési hely:</span><span class="modalinfo"> {{$szeged->szulhely}}</span><br>

                                        <span class="alma">Csillagjegy:</span><span class="modalinfo"> {{$szeged->csillagjegy}}</span><br>
                                        <span class="alma">Családiállapot:</span><span class="modalinfo"> {{$szeged->csaladiallapot}}</span><br>
                                        <span class="alma">Előző klubbok:</span><span class="modalinfo"> {{$szeged->elozoklubbok}}</span><br>
                                        <span class="alma">Eredmények:</span> <span class="modalinfo">{{$szeged->valogatottsag}}</span><br>
                                        <span class="alma">Hobbi:</span><span class="modalinfo"> {{$szeged->hobbi}}</span><br>
                                        <span class="alma">Ország:</span><span class="modalinfo"> {{$szeged->orszag}}</span><br>

                                        <span class="alma">Város:</span><span class="modalinfo"> {{$szeged->varos}}</span><br>
                                        <span class="alma">Csapat:</span><span class="modalinfo"> {{$szeged->csapat}}</span><br>
                                        <span class="alma">Játékos:</span><span class="modalinfo"> {{$szeged->jatekos}}</span><br>
                                        <span class="alma">Színész:</span> <span class="modalinfo">{{$szeged->szinesz}}</span><br>
                                        <span class="alma">Autó:</span><span class="modalinfo"> {{$szeged->auto}}</span><br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
       @php $i++; @endphp
        @endforeach
    </div>



    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="szakmaistab">
                <h1>Szakmai stáb:</h1>
                <p><span class="alma">Vezetőedző:</span> Juan Carlos Pastor<br>
                    <span class="alma">Másodedző:</span> Marko Krivokapics<br>
                    <span class="alma">Masszázs-terapeuta:</span> Dorde Ignjatovic<br>
                    <span class="alma">Masszázs-terapeuta:</span> Molnár Tamás<br>
                    <span class="alma">Csapatorvos:</span> Dr. Szabó István<br>
                    <span class="alma">Kapusedző:</span> Nenad Damjanovic<br>
                    <span class="alma">Erőnléti edző:</span> Bubori Kornél<br>
                </p>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="sikerek">
                <h1>Elért eredmények:</h1>
                <p><span class="alma">Magyar bajnoki arany:</span> 3 <i class="fas fa-trophy"></i><br>
                    <span class="alma">Magyar bajnoki ezüst:</span> 17 <i class="fas fa-trophy"></i><br>
                    <span class="alma">Magyar bajnoki bronz:</span> 10 <i class="fas fa-trophy"></i><br>
                    <span class="alma">Magyar kupa arany:</span> 6 <i class="fas fa-trophy"></i><br>
                    <span class="alma">Magyar kupa ezüst:</span> 11 <i class="fas fa-trophy"></i><br>
                    <span class="alma">EHF kupa:</span> 1 <i class="fas fa-trophy"></i><br>
                    <span class="alma">EHF Bajnokok Ligája-negyeddöntő:</span> 3 <i class="fas fa-trophy"></i><br>
                </p>
            </div>
        </div>
        <div class="col-12">
            <div class="altinfo">
                <h1>A klubbról röviden:<img id="logo2" src="{{ asset('images/pick/picklogo.png') }}"></h1>
                <p>
                    MOL-Pick Szeged (korábban Szegedi Előre, Szegedi Volán, Tisza Volán, majd Pick Szeged) férfi kézilabda klub.
                    Jogelődjét, a Szegedi Előrét 1961. december 16-án alapították, és innen vált a csapat az ország és Európa egyik leghíresebb férfi kézilabda csapatává.
                </p>
                <p>
                    A 2016–2017-es idényben a Bajnokok Ligája-sorozat igazán emlékezetesre sikerült, ismét Európa legjobb nyolc csapata közé jutottunk.
                    A Paris SG állított meg bennünket, a szegedi vereség után a francia fővárosban döntetlent játszottunk. Debrecen nem hozott szerencsét,
                    az mk-döntőben egy utolsó pillanatban értékesített hetessel kaptunk ki 23–22-re a Veszprémtől. A döntő első meccsén hiába vezettünk – az alapszakaszt az első helyen zártuk –,
                    végül a Telekom Veszprém 23–17-re nyert, és hiába hoztuk 30–27-re a szegedi meccset, összesítésben a nagy rivális bizonyult a legjobbnak.
                <p>
                    1996, 2007, 2018. A sorminta folytatódott. 2018-ban harmadik bajnoki címünket szereztük meg. Egy nem igazán emlékezetes őszi produkció után ültünk fel a trónra.
                    Az alapszakaszt a második helyen zártuk, így otthon kezdtük a döntőt. Fantasztikus hangulatban 32–28-ra nyertünk, és ebből az előnyből egyet megőrizve (29–26) megszereztük
                    az aranyérmet. A Dóm téren 6 ezer MOL-PICK Szeged-szurkolóval ünnepeltünk. A szokásos debreceni magyarkupa-döntőre az adidas #Kékek Tour jóvoltából közel 1500 szurkoló kísért
                    el bennünket, ez új idegenbeli nézőcsúcs volt. A döntőben a Veszprém 23–21-re győzött le bennünket. A BL-ben a legjobb 16 között búcsúztunk, immár harmadjára vert ki bennünket a THW Kiel.
                </p>

            </div>
        </div>
    </div>
</div>
@endsection



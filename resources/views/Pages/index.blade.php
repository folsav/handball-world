@extends('layout')
@section('title')
    Handball World
@endsection

@section('content')
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="z-index: -1">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{ asset('/images/vb.jpg')}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('/images/vb2.jpg')}}" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('/images/valami2.jpg')}}" alt="Third slide">
            </div>
        </div>
    </div>
    <section class="indexelsosection">
        <div class="container">
            <div class="row indexsection">
                <div class="col-md-3 col-sm-6 indexikonok">
                    <i class="far fa-newspaper" aria-hidden="true"></i>
                    <h4>Friss hírek</h4>
                    <p>Minden nap frissülő hírek hazánk kézilabda eseményeiről. </p>
                </div>
                <div class="col-md-3 col-sm-6 indexikonok">
                    <i class="fas fa-file-alt"></i>
                    <h4>Aktív fórum</h4>
                    <p>Rengetek témában meg tudod vitatni nézeteid a többi kézilabdafanatikussal. </p>
                </div>
                <div class="col-md-3 col-sm-6 indexikonok">
                    <i class="fas fa-info"></i>
                    <h4>Általános információk</h4>
                    <p>Itt mindent megtudhatsz ami kézilabda. </p>
                </div>
                <div class="col-md-3 col-sm-6 indexikonok">
                    <i class="fas fa-shopping-cart"></i>
                    <h4>Web shop</h4>
                    <p>Vásárold meg kedvenc csapataid mezeit sáljait. </p>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="titlehirek">
            <h2>Hírek</h2>
            <br>
        </div>
        <div class="row">
            @php $i=0; @endphp
            @foreach ($items as $item)
                @if($loop->index < 3)
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="item">
                        @if ($category = $item->get_category())
                            @if(stristr($category->get_label(), 'Győr') or stristr($category->get_label(), 'Danyi'))
                                <img src="{{ asset('images/hirek/eto.jpg') }}">
                            @elseif(stristr($category->get_label(), 'veszprém') or stristr($category->get_label(), 'Davis'))
                                <img src="{{ asset('images/hirek/veszprem2.jpg') }}">
                            @elseif(stristr($category->get_label(), 'ftc') or stristr($category->get_label(), 'Elek'))
                                <img src="{{ asset('images/hirek/ftc.jpg') }}">
                            @elseif(stristr($category->get_label(), 'MOL') or stristr($category->get_label(), 'Pastor'))
                                <img src="{{ asset('images/hirek/mol.jpg') }}">
                            @else
                                <img src="{{ asset('images/hirek/labda.jpg') }}">
                            @endif
                        @endif
                        <h3><a href="" data-toggle="modal" data-id="{{$item->get_title()}}" data-target="#exampleModal-{{$i}}">{{ $item->get_title() }}</a></h3>

                    </div>
                </div>
                @endif
                <div class="modal" id="exampleModal-{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"></h5>
                                <h2><a href="{{ $item->get_permalink() }}">{{ $item->get_title() }}</a></h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-5 hirszoveg">
                                        @if ($category = $item->get_category())
                                            @if(stristr($category->get_label(), 'ETO') or stristr($category->get_label(), 'Danyi'))
                                                <img src="{{ asset('images/hirek/eto.jpg') }}">
                                            @elseif(stristr($category->get_label(), 'ftc') or stristr($category->get_label(), 'Elek'))
                                                <img src="{{ asset('images/hirek/ftc.jpg') }}">
                                            @elseif(stristr($category->get_label(), 'MOL') or stristr($category->get_label(), 'Pastor'))
                                                <img src="{{ asset('images/hirek/mol.jpg') }}">
                                            @elseif(stristr($category->get_label(), 'Veszprém') or stristr($category->get_label(), 'Davis'))
                                                <img src="{{ asset('images/hirek/veszprem.png') }}">
                                            @else
                                                <img src="{{ asset('images/hirek/labda.jpg') }}">
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-lg-7 hirszoveg">
                                        <p>{{ $item->get_description() }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-between">
                                <p>Közzétéve {{ $item->get_date('Y F j | g:i a') }}</p>
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Mégse</button>
                            </div>
                        </div>
                    </div>
                </div>
                @php $i++; @endphp
            @endforeach
        </div>
    </div>
    <section class="sectionketto">
        <div class="container">
            <div class="titleshop">
                <h2>Webshop</h2>
                <br>
            </div>
            <div class="feature_row row">
                <div class="col-md-12 col-lg-6 feature_img">
                    <img src="{{ asset('images/shop12.jpg') }}" alt="">
                </div>
                <div class="col-md-12 col-lg-6 sectionketto_content">
                    <div class="subtittle">
                        <h2>TERMÉKEINK</h2>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <i class="fas fa-user-tie" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="#">SÁLAK</a>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting indus-try. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <i class="fas fa-tshirt" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="#">MEZEK</a>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting indus-try. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <i class="fas fa-volleyball-ball" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <a href="#">EGYÉB KIEGÉSZÍTŐK</a>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting indus-try. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection




@extends('layout')
@section('title')
    Siófok
@endsection
@section('menu')
    Csapatok
@endsection
@section('cim')
    Siófok KC
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="bevezeto">
                <h1>Játékosok:<img id="logo" src="{{ asset('images/sio/logo.png') }}"></h1>
            </div>
        </div>
    </div>

    <div class="row">
        @php $i=0; @endphp
        @foreach($Siofok as $siofok)
            <div class="content col-lg-3 col-md-6 col-sm-12">
                <div class="card middle">
                    <div class="front">
                        <img src="{{ asset('images/sio') }}/{{$siofok->kep}}">
                    </div>
                    <div class="back">
                        <div class="back-content middle">
                            <div class="sm">
                                <p> <span class="alma">Név:</span> {{$siofok->nev}}<br>
                                    <span class="alma">Poszt:</span> {{$siofok->poszt}}<br>
                                    <span class="alma">Állampolgárság:</span> {{$siofok->nemzet}}<br>
                                    <span class="alma">Kor:</span> {{$siofok->kor}}<br>
                                    <span class="alma">Magasság:</span> {{$siofok->magassag}} cm<br>
                                    <span class="alma">Születési idő:</span> {{$siofok->szulido}}<br>
                                    <span class="alma">Születési hely:</span> {{$siofok->szulhey}}<br>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <h3><a href="" data-toggle="modal" data-id="{{$siofok->id}}" data-target="#exampleModal-{{$i}}">{{$siofok->nev}}</a></h3>
            </div>
            @php $i++; @endphp
        @endforeach
    </div>

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="szakmaistab">
                    <h1>Szakmai stáb:</h1>
                    <p><span class="alma">Vezetőedző:</span> Tor Odvar Moen<br>
                        <span class="alma">Másodedző:</span> Bent Dahl<br>
                        <span class="alma">Masszázs-terapeuta:</span> Bimbó Tamás<br>
                        <span class="alma">Rehabilitációs edző:</span> Vaskó Réka<br>
                        <span class="alma">Csapatorvos:</span> Dr. Kisegyházi Attila<br>
                        <span class="alma">Kapusedző:</span> Pastrovics Melinda<br>
                        <span class="alma">Erőnléti edző:</span> Slobodan Acimov<br>
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="sikerek">
                    <h1>Elért eredmények:</h1>
                    <p><span class="alma">Magyar bajnoki bronz:</span> 1 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar bajnoki 2. osztály arany:</span> 2 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar bajnoki 3. arany:</span> 2 <i class="fas fa-trophy"></i><br>
                        <span class="alma">Magyar kupa bronz:</span> 1 <i class="fas fa-trophy"></i><br>
                        <span class="alma">EHF kupa 8-ad döntő:</span> 1 <i class="fas fa-trophy"></i><br>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="altinfo">
                    <h1>A klubbról röviden:<img id="logo" src="{{ asset('images/sio/logo.png') }}"></h1>
                    <p>
                        Az Siófok KC egy siófoki női kézilabdacsapat, és az egyetlen Somogy megyei NB I-es női kézilabda csapat.
                        Néhány évvel ezelőtt még a kézilabda harmadik vonalában szerepelt, majd a bajnoki cím megszerzésével feljutott az NBI/B-be.
                        Az NBI/B-ben két évet töltött a klub, majd 2009-ben kapott egy olyan felkérést, hogy amennyiben az egyesület biztosítja a feltételeket, akkor a
                        2009/2010-es bajnoki idényben az élvonalban szerepelhet a csapat. A klub történelmi sikert ért el a 2011/2012-es szezonban, ahol a bajnokságban 3. helyen, a
                        Magyar Kupában 4. helyet értek el.
                    </p>
                    <p>
                        A Siófok KC történetében több szempontból is a 2014/2015 szezon hozta meg a nagy áttörést.
                        A klub első alkalommal szerepelt a Kupagyőztesek Európa Kupájában, sikerült olyan világsztárokat leigazolni,mint a brazil világbajnok Daniela Piedade vagy
                        a német válogatott irányító Nina Wörz. 2014.november 7-én ünnepélyes keretek között lerakták a Kiss Szilárd Aréna alapkövét, elkezdődött jövendőbeli otthonunk
                        építése. A várható átadás 2016 nyara. Sportszakmai szempontból a klub vezetés úgy döntött,hogy rálép az"északi" útra és 2015.áprilisában a világhírű Christian
                        Dalmose-t nevezte ki vezetőedzőnek, aki a bajnokság rájátszásában 100%-os mérleggel az EHF-Kupa indulást érő 5. helyre kormányozta a csapatot.
                    <p>
                        A Siófok KC életében nagyon nagy hangsúly van az utánpótlás képzésen,melynek szakmai igazgatója Kiss Szilárd. Az ő vezetésével és edzői iránymutatásával már több,
                        mint 200 gyerek kézilabdázik klubunkban. Hosszú távú cél, hogy saját nevelésű játékosok is szerepeljenek a felnőtt keretben ,ezzel is növelve a csapat identitását.
                    </p>

                </div>
            </div>
        </div>
    </div>


@endsection

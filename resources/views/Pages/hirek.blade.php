@extends('layout')
@section('title')
    Hírek
@endsection
@section('menu')
    Hírek
@endsection
@section('cim')
    Hírek
@endsection
@section('content')

    <div class="container">
        <div class="row">
            @php $i=0; @endphp
            @foreach ($items as $item)
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="item">
                        @if ($category = $item->get_category())
                            @if(stristr($category->get_label(), 'Győr') or stristr($category->get_label(), 'Danyi'))
                                <img src="{{ asset('images/hirek/eto.jpg') }}">
                            @elseif(stristr($category->get_label(), 'veszprém') or stristr($category->get_label(), 'Davis'))
                                <img src="{{ asset('images/hirek/veszprem2.jpg') }}">
                            @elseif(stristr($category->get_label(), 'ftc') or stristr($category->get_label(), 'Elek'))
                                <img src="{{ asset('images/hirek/ftc.jpg') }}">
                            @elseif(stristr($category->get_label(), 'MOL') or stristr($category->get_label(), 'Pastor'))
                                <img src="{{ asset('images/hirek/mol.jpg') }}">
                            @else
                                <img src="{{ asset('images/hirek/labda.jpg') }}">
                            @endif
                        @endif
                        <h3><a href="" data-toggle="modal" data-id="{{$item->get_title()}}" data-target="#exampleModal-{{$i}}">{{ $item->get_title() }}</a></h3>

                    </div>
                </div>
                <div class="modal" id="exampleModal-{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"></h5>
                                <h2><a href="{{ $item->get_permalink() }}">{{ $item->get_title() }}</a></h2>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-5 hirszoveg">
                                        @if ($category = $item->get_category())
                                            @if(stristr($category->get_label(), 'ETO') or stristr($category->get_label(), 'Danyi'))
                                                <img src="{{ asset('images/hirek/eto.jpg') }}">
                                            @elseif(stristr($category->get_label(), 'ftc') or stristr($category->get_label(), 'Elek'))
                                                <img src="{{ asset('images/hirek/ftc.jpg') }}">
                                            @elseif(stristr($category->get_label(), 'MOL') or stristr($category->get_label(), 'Pastor'))
                                                <img src="{{ asset('images/hirek/mol.jpg') }}">
                                            @elseif(stristr($category->get_label(), 'Veszprém') or stristr($category->get_label(), 'Davis'))
                                                <img src="{{ asset('images/hirek/veszprem.png') }}">
                                            @else
                                                <img src="{{ asset('images/hirek/labda.jpg') }}">
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-lg-7 hirszoveg">
                                        <p>{{ $item->get_description() }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-between">
                                <p>Közzétéve {{ $item->get_date('Y F j | g:i a') }}</p>
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Mégse</button>
                            </div>
                        </div>
                    </div>
                </div>
                @php $i++; @endphp
            @endforeach
        </div>

    </div>

@endsection

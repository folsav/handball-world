@extends('layout')
@section('menu')
    Belépés
@endsection
@section('cim')
Új jelszó
@endsection
@section('content')

    <div class="login" style="padding-bottom:110px">
        <div class="form">
                <div id="login">
                    <h1>Elfelejtett jelszó</h1>

                    <form action="{{ route('password.email') }}" method="POST">
                        @csrf
                        <div class="field-wrap">
                            <label for="email">
                                E-mail cím<span class="req">*</span>
                            </label>
                            <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif

                        </div>
                        <button type="submit" class="button button-block">Új jelszó küldése</button>
                    </form>
                </div>
        </div> <!-- /form -->
    </div>
@endsection

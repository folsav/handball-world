@extends('layout')
@section('menu')
    Belépés
@endsection
@section('cim')
<div id="content-div-2">
    Bejelentkezés
</div>
<div id="content-div-3" style="display: none">
    Regisztráció
</div>
@endsection
@section('content')

        <div class="login">
                <div class="form">

                    <ul class="tab-group">
                        <li id="show" class="tab active"><a href="#login">Bejelentkezés</a></li>
                        <li id="show2" class="tab"><a href="#signup">Regisztráció</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="login">
                            <h1>Bejelentkezés</h1>

                            <form action="{{ route('login') }}" method="POST">
                                @csrf
                                <div class="field-wrap">
                                    <label for="email">
                                        E-mail cím<span class="req">*</span>
                                    </label>
                                    <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="off">
                                    @if ($errors->has('email'))
                                        <strong style="color: red">Hibás e-mail vagy jelszó!</strong>
                                    @endif
                                </div>

                                <div class="field-wrap">
                                    <label for="password">
                                        Jelszó<span class="req">*</span>
                                    </label>
                                    <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"required autocomplete="off"/>
                                    @if ($errors->has('password'))
                                        <strong style="color: red">Hibás e-mail vagy jelszó!</strong>
                                    @endif
                                </div>

                                @if (Route::has('password.request'))
                                <p class="forgot">
                                    <a href="{{ route('password.request') }}">Elfelejtetted a jelszavad?</a>
                                @endif
                                    <a href="{{ route('guestcheckout.index') }}" style="float: left">Fizetés vendégkénet.</a>
                                </p>
                                <button type="submit" class="button button-block">Bejelentkezés</button>
                            </form>
                        </div>


                        <div id="signup">
                            <h1>Regisztráció</h1>

                            <form action="{{ route('register') }}" method="POST">
                                @csrf
                                <div class="field-wrap">
                                    <label for="name">
                                        Felhasználó név<span class="req">*</span>
                                    </label>
                                    <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"required autocomplete="off"/>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="field-wrap">
                                    <label for="email">
                                        E-mail cím<span class="req">*</span>
                                    </label>
                                    <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required/>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="field-wrap">
                                    <label for="password">
                                        Jelszó<span class="req">*</span>
                                    </label>
                                    <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required/>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="field-wrap">
                                    <label for="password-confirm">
                                        Jelszó ismét<span class="req">*</span>
                                    </label>
                                    <input id="password-confirm" type="password"  name="password_confirmation" required/>
                                </div>

                                <button type="submit" class="button button-block">Regisztráció</button>

                            </form>

                        </div>

                    </div><!-- tab-content -->

                </div> <!-- /form -->
        </div>
<script>
        $(document).ready(function(){
        $("#show").click(function(){
        $("#content-div-2").show();
            $("#content-div-3").hide();
        });
        $("#show2").click(function(){
        $("#content-div-3").show();
            $("#content-div-2").hide();
        });
        });
</script>
@endsection


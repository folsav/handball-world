@extends('layout')
@section('menu')
    Belépés
@endsection
@section('cim')
    E-mail megerősítés
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center " style="padding-bottom:290px; padding-top: 50px">
            <div class="card" style="width: 60%; height: 200px;">
                <div class="card-header" style="background-color: #222629;color: white">{{ __('E-Mail Megerősítés') }}</div>
                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Az új e-mail megerősítőt elküldtük az e-mail címedre.') }}
                        </div>
                    @endif

                        {{ __('Mielőtt elkezdenéd a böngészést erősítsd meg az e-mail címed.') }}
                        {{ __('Ha nem kaptad meg az e-mailt') }}, <a href="{{ route('verification.resend') }}" style="color: #86C232">{{ __('klikkelj ide egy másik küldéséhez') }}</a>.
                </div>
            </div>
    </div>
</div>
@endsection

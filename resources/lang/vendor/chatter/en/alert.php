<?php

return [
    'success' => [
        'title'  => 'Well done!',
        'reason' => [
            'submitted_to_post'       => 'Hozzászólás sikeresen hozzáadva!',
            'updated_post'            => 'Sikeres szerkesztés!',
            'destroy_post'            => 'Sikeres törlés!',
            'destroy_from_discussion' => 'Sikeres törlés!',
            'created_discussion'      => 'Sikeresen létrehoztál egy topikot! ',
        ],
    ],
    'info' => [
        'title' => 'Kezeket fel!',
    ],
    'warning' => [
        'title' => 'Wuh Oh!',
    ],
    'danger'  => [
        'title'  => 'Oh Ne!',
        'reason' => [
            'errors'            => 'Fixálja a következő hibákat:',
            'prevent_spam'      => 'In order to prevent spam, please allow at least :minutes in between submitting content.',
            'trouble'           => 'Elnézést hiba történt!',
            'update_post'       => 'Nah ah ah... Could not update your response. Make sure you\'re not doing anything shady.',
            'destroy_post'      => 'Nah ah ah... Could not delete the response. Make sure you\'re not doing anything shady.',
            'create_discussion' => 'Whoops :( There seems to be a problem creating your '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'title_required'    => 'Adja meg a címet',
            'title_min'		    => 'A címnek minimum :min'.' karakternek kell lennie!',
            'title_max'		    => 'A cím maximum :max'.' karakter lehet!',
            'content_required'  => 'Üresen maradt a szövegdoboz!',
            'content_min'  		=> 'A szövegnek minimum :min karaktert tartalmaznia kell!',
            'category_required' => 'Válaszz kategóriát!',



        ],
    ],
];

<?php

return [
    'words' => [
        'cancel'  => 'Mégse',
        'delete'  => 'Törlés',
        'edit'    => 'Szerkesztés',
        'yes'     => 'Igen',
        'no'      => 'Nem',
        'minutes' => '1 minute| :count minutes',
    ],

    'discussion' => [
        'new'          => 'Új '.trans('chatter::intro.titles.discussion'),
        'all'          => 'Az összes '.trans('chatter::intro.titles.discussion'),
        'create'       => 'Új '.trans('chatter::intro.titles.discussion'),
        'posted_by'    => 'Közzétette',
        'head_details' => 'Posztolva ebbe a kategóriába',

    ],
    'response' => [
        'confirm'     => 'Biztos vagy benne hogy törölni szeretnéd?',
        'yes_confirm' => 'Igen törölni szeretném',
        'no_confirm'  => 'Nem, köszönöm!',
        'submit'      => 'Szólj hozzá!',
        'update'      => 'Frissítés',
    ],

    'editor' => [
        'title'               =>  trans('chatter::intro.titles.discussion').' címe',
        'select'              => 'Válassz kategóriát',
        'tinymce_placeholder' => 'Írd ide a '.trans('chatter::intro.titles.discussion').'ot....',
        'select_color_text'   => 'Válassz színt hozzá '.trans('chatter::intro.titles.discussion').' (optional)',
    ],

    'email' => [
        'notify' => 'Notify me when someone replies',
    ],

    'auth' => 'Please <a href="/:home/login">login</a>
                or <a href="/:home/register">register</a>
                to leave a response.',

];
